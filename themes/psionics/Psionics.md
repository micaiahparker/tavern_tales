# Psionics

A mind is a terrible thing to waste. This is especially true when you can use your mind to read thoughts, levitate objects, and force choke all who dare oppose you. Such is the power of psions, who turn their energy inward to unlock the infinite potential of their own minds. They sharpen their wit the same way warriors sharpen their blades, transforming their raw intellects into deadly weapons capable of crushing skulls with a simple thought. They fortify their willpower the same way a blacksmith tempers steel armor, building an impenetrable defense that transcending physical limitations. Look inside yourself, psion, and behold the awesome power that lies dormant within you. What fragments of your power will you awaken first?

* A quiet maid reads the minds of her royal employers, selling their darkest secrets to the highest bidder.

* Born small and frail, a young psion learns how to levitate swords and becomes the most feared duelist in the land.

* A powerful psychic travels from plane to plane hoping to unlock the secret of the universe.

## Combat

### Archon

Whenever you want, you can become completely invulnerable to all harm for a few seconds. Then, the GM gives you a condition. Archon can't make you immune to conditions you acquire through this trait.

* *Describe*: Your skin taking the color of a starry night, becoming pure energy, showing your true form.

### Confusion

You can give someone the condition "Confused" for several minutes. Confused creatures have difficulty distinguishing friend from foe.

* *Describe*: Disrupting their natural brain waves, filling their minds with nightmarish thoughts. 
* *Erratic Behavior*: Attack an ally, move somewhere disadvantageous, throw away something valuable.

### Force Wave

You can push everyone in a zone a close distance away, or one creature a far distance away.

* *Describe*: Releasing a shock wave, emitting a psi pulse, unleashing an explosion of telekinetic energy.

### Lobotomize

You can attack a creature and describe one of its capabilities. If the creature has any traits associated with that capability, they lose 1 of those traits for several hours. 

* *Describe*: Suppressing their memories, preventing their mind from acting, locking their abilities with magic

### Projection

You can take actions as if you're standing in any far space.

* *Describe*: Animating your weapon, projecting a psychic version of yourself, bending reality with your will.

### Reverse Trajectory
Collaborate on how you charge this trait. Spend the charge to send a ranged attack back at the attacker.

* *Describe*: Using telekinesis to send back an arrow, unleashing psychic recoil, absorbing power to shoot it back.

## Exploration

### Astral Projection

You can travel to another dimension or plane. You may bring close creatures with you. Collaborate on what other dimensions or planes exist.

* *Describe*: Your spirit leaving your body, going on a dream-quest, stepping into the nonphysical realm.
* *Planes*: Astral Plane, Dreamscape, Shadow Realm, a person's subconscious mind, heaven, hell, the afterlife.

### Perfect Mind

You have a perfect memory, and you can instantly complete complex mental exercises.

* *Describe*: Closing your eyes to think for a moment, copying something into your brain, being a genius.
* *Instantly*: Perform a calculation, create an elaborate story, read a book, write a play in your head.

### Pre/Postcognition

You can experience the future and/or past. Collaborate on how this trait works. 

* *Describe*: Receiving a vision, gazing through the time-stream, calculating the sequence of cause and effect.

### Sensory Link

You can forge a link with a creature you're touching. For the next day or so, you can experience the world through their senses whenever you want.

* *Describe*: Your eyes changing to their color, seeing and hearing things that aren't present, feeling auras.

### Telekinesis

You can exert telekinetic force. Collaborate on how this trait works.

* *Describe*: Manipulating mass, concentrating intently, exerting force with your mind, changing gravity.
* *How it Works*: You can't lift more than your weight, precision requires intense concentration, it's exhausting.

## Interaction

### Mind Control

You can control the mind of a visible creature that isn't significantly more powerful than you. Collaborate on how this trait works.

* *Describe*: Overpowering their pathetic will, enslaving their mind, implanting your thoughts and desires.
* *How it Works*: They struggle to break free, it gives you a crippling headache, you can't move while doing it.

### Brainwash

You can add or remove a memory or personality trait from someone you're touching. You can't make them forget how to do things (talk, read, cast spells, etc).

* *Describe*: Devouring  a memory, deleting a brain wave, brainwashing them, cutting out part of their mind.

### Read Mind

You can hear visible creatures' thoughts. Collaborate on how this trait works.

* *Describe*: Listening to their thoughts, leeching their brain waves, seeing their dreams, feeling their desires.

### Telepathy

You can telepathically communicate with far creatures. 

* *Describe*: Sharing a collective consciousness, hearing what each other says, speaking via a shared mind-cord.

### Share Memory

You can share memories with creatures you're touching. They gain any of your memories you like, and you gain any of there memories they like. 

* *Describe*: Accessing their subconscious, dreaming their dreams, giving them a vision of what you experienced.
