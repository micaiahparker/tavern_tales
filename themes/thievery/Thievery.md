# Thievery

Some fight for honor. Others fight for a cause. But thieves? They're motivated by one thing: precious, precious gold. The world is filled with powerful magic items and glittering gemstones. Why should they be left to gather dust in some ancient tomb? All you have to do is pick a few locks, sneak past a group of oblivious guards, disarm a trap or two, and untold riches could be yours! What marvelous treasures could await you in your adventures?

* An elite assassin sneaks through the shadows, studying his marks from afar before striking.

* A pirate queen uses dirty tricks the element of surprise to catch merchant ships unaware.

* A street urchin nimbly darts through a crowd, relieving people of their heavy coins.

## Combat

### Assassin

You can have the GM tell you all of a visible creature's weaknesses.

* *Describe*: Learning their habits and routines, finding an exploitable weakness, studying their body language.

### Duck

Collaborate on how you charge this trait. Spend the charge to make an attack meant for you hit the next most logical target instead.

* *Charge*: Get safely out of danger, frame someone for your crime, have a drink and talk about your close call.
* *Describe*: Leaping out of the way, pulling an enemy in front of you, ducking and covering your head.

### Misdirection

If you like, enemies can't attack you as long as there are other viable targets.

* *Describe*: Looking nonthreatening, drawing attention to someone else, hiding behind an ally, running away.

### Sneak Attack

When you attack an unsuspecting and vulnerable victim, you can defeat them however you want. If they're significantly more powerful than you, give them a brutal physical condition instead.

* *Brutal Conditions*: Slit throat, broken bone, gouged-out eyes, bloody amputation, severed arteries.
* *Describe*: Knocking them over the head, choking them out, stabbing them in the back, slitting their throat.

### Tumble

Gain the defense "Tumble []." Mark it when you suffer a Bad Tale that you can logically dodge. When you do, remove or temporarily suppress (whichever is more logical) a condition that restricts your movement, and you can immediately move.

* *Describe*: Diving out the way, running for safety, swinging away on a rope or vine, ducking into a ditch.

## Exploration

### Cloaked in Shadows

Gain the defense "Cloaked in Shadows []." Mark it when you suffer a Bad Tale while hidden or obscured. Then, you vanish; not even the GM knows where you are. On your next turn, tell the GM where you're hiding (though you can stay hidden in your new location if you like). 

* *Describe*: Ducking into the shadows, taking a glancing blow from their bad aim, repositioning to a better spot.

### Escape

You can escape your current predicament and bring as many others as you like with you. The GM gives you a condition based on how you escaped. You can't use this again until you lose that condition.

* *Describe*: Vanishing in a cloud of smoke, pulling a lever to reveal an escape tunnel, disappearing in the crowd.

### Eye for Treasure

When you find treasure, find 1 extra treasure.

* *Describe*: Finding extra hidden valuables, getting a good price for your treasure, pocketing valuables.

### From the Shadows

Once each time while hiding, you can perform an action that would normally break stealth and reveal your location without breaking stealth. Absurdly obvious actions (jumping on a table and yelling) will reveal you as normal.

* *Describe*: Returning to the shadows, doing something before anyone notices, waiting for the perfect moment.

### Hideout

You can establish an unpopulated area as your hideout. Your hideout is much harder than normal to locate. Collaborate on why it's so difficult to locate.

* *Describe*: Establishing a false front, installing a hidden door, hiding it in a hard-to-spot alley or cul-de-sac.

### Silence and Shadow

If you like, everything you do is completely noiseless. Also, you can dim or extinguish a visible light source until someone takes the time to rekindle the light. You can't dim celestial bodies, such as the sun.

* *Describe*: Landing like a cat, snuffing out a candle, using shadow-magic on a torch, spreading gloom.

### Stash

When you enter a new region, tell the GM where there's a hidden stash. If you reach the stash, it has supplies that happen to be useful for your current predicament.

* *Describe*: Digging up buried treasure, looting a secret thieves' guild stash, hiding away supplies for later.

### Trap Finder

Gain the defense "Trap Finder []." Mark it when you suffer a Bad Tale related to traps. Also, the GM must answer honestly whenever you ask if there's anything hidden here.

* *Describe*: Searching for hidden panels, relying on your acute sense of touch, listening for a soft "click."

### Without a Trace

If nobody has seen you for several minutes, you can vanish without a trace. Nobody knows where you are (including the GM), so you skip all of your turns. At any point, reappear anywhere that you logically could have reached during that time.

* *Describe*: Throwing down a smoke bomb, disappearing into a forest, emerging when they least expect it.

## Interaction

### Black Market

Whenever you want, you can have safe and discreet access to criminal resources.

* *Describe*: Hearing whispers, reading thieves' symbols carved into buildings, leveraging back alley contacts.
* *Resources*: Fence, lookouts, assassin, thugs for hire, informant, smugglers, poisons, controlled substances.

### Everything Has a Price

You can spend 1 treasure to gain 1 Good Tale. If there's no one to pay when you use this, explain how a purchase you made long ago is now paying off. 

* *Describe*: Greasing palms, bribing officials, hiring a professional, putting a scheme into motion.

### Mastermind

You can orchestrate a grand plan (max 1), giving as many allies as you want a job related to your plan. Write their jobs on your character sheet. When one of them fulfills their job, check their job. Cross off a checked job to gain a free Good Tale toward executing your grand plan.

* *Describe*: Organizing the score of a lifetime, tricking others to do the dirty work, your criminal genius.
* *Jobs*: Take out the guards, discover the pass code, steal the gate keys, stand guard, secure an escape route.

### Second Identity

Collaborate on a complete second identity for yourself. 

* *Describe*: Changing your accent, walking differently, roleplaying a completely different personality.
* *Second Identity*: Name, family friends, contacts, paperwork, home, reputation, history, wardrobe, job.

### Spy Network

You have an extensive spy network. Collaborate on what they're targeting. Your spy network can provide you with intimate, useful information about their target whenever you want. You can change their target, but it takes time and/or resources.

* *Describe*: Meeting cloaked figures, getting a package from a drop-off zone, and NPC slipping you a letter.
* *Targets*: A certain city, politicians and nobles, armies and troops, the elves, pirates, merchants.

### Sudden but Inevitable Betrayal

You can reveal that someone who isn't significantly more powerful than you was secretly your minion all along.

* *Describe*: Giving the secret signal, nodding at your hidden ally, executing your master plan, shouting "NOW!"

### Tamper with Evidence

You can lose all heat you and your allies have for a crime. If you have an item that belongs to someone else, you can convincingly frame that person for the crime or event instead.

* *Describe*: Wiping down finger prints, destroying clues, planting evidence, misleading investigators.

### White Lie

You can convince others of lies that would normally be unbelievable. Collaborate on what now constitutes a believable lie for you.

* *Describe*: Telling a white lie, manipulating someone with your wit, leading them to assume the wrong thing.
