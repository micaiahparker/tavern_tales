
# Artifice

Soldiers often say that the sword doesn't make the man, arguing that skill at arms is more important than strength of steel. Artificers roll their eyes at that statement or at least, they would if they weren't too busy carving magic runes into a indestructible sword. Or setting enchanted rubies into a suit of armor that renders the wearer immune to fire. Or placing gears into a steam-powered bull large enough to level a fortress. Those who doubt the power of  craftsmanship have clearly never experienced the beauty of creation. Truly, a single act of creation can change the world. How will you leave your mark? Will you build something that world has never before seen? Will you forge a sword capable of slaying dragons? Will you construct thick city walls to protect those you love? Fire up the forges, young artisan, and prove to the gods you, too, have mastered the art of creation!

* A legendary blacksmith carves ancient runes into weapons, imbuing them with powerful magic.

* After losing his arm to a dragon, a warrior builds a clockwork arm for himself and sets out to even the score.

* A mad scientist experiments with bizarre gadgets and gizmos, pushing the limits of science and morality.

## Combat

### Armor Plating

Defenses you gain from armor and shields each grant 1 additional box. 

* *Describe*: Steam-powered absorption plates, glowing symbols, thick chainmail, dragon-scale armor plating. 

### Arsenal

Draw a symbol next to 3 of your inventory slots. Items you place in these slots vanish from reality (but still occupy slots). No one else can access these items. Whenever you want, you can call any of these items to your person, or make them vanish again.

* *Describe*: A sword appearing in your hand, summoning your hammer from the heavens, morphin' time. 

### Deflect

At any point, you can break one of your weapons, shields, or armor worth at least 1 treasure to completely avoid a Bad Tale related to being attacked. 

* *Describe*: Sparks flying when blades clash, your ablative armor, taking the hit with your shield.

### Dismantle

You can destroy a personal item, such as a weapon or suit of armor.

* *Describe*: Striking a structural weak point, breaking it in half, matching its vibration frequencies. 

### Enchant

You can give a touched item 1 item trait (max 3) as a temporary trait for several hours. 

* *Describe*: Etching runes on its surface, treating it with magic oils, altering its molecular lattice structure.

### War Machine

You can spend materials to create a war machine with half your XP. It's inert without a pilot. The pilot uses the machine's attributes instead of their own. 

* *Describe*: Steam-powered gears, shining metal, smoke billowing from a furnace, blinking lights. a console.
* *Machines*: Power armor, steam-powered catapult, submarine, elemental cannon, clockwork steed, spider tank.
* *Materials*: Scrap metal, saw blades, shields and armor plating, rubber tubes, copper wires, gears, gizmos.

### Warhead

You can spend materials to create a warhead. Once activated, it gathers power for about 1 minute and then explodes, causing massive damage out to a far distance. Only one attempt can be made to disarm it.

* *Describe*: Bundle of explosives, glowing spell-bomb, antimatter emitter, napalm tank, portable black hole.
* *Gathers Power*: Ominous humming that grows louder, burning fuse, running hourglass, deep rumbling.
* *Materials*: Oil, high explosives, firing pins, wire, metal casing, unstable chemicals, magic runes, gunpowder.

## Exploration

### Forge

You can spend materials to create a magic item, or improve an existing item. Collaborate on the item's properties. Stronger materials create more powerful items.

* *Describe*: Hammering metal at a forge, performing the magic item naming ritual, imbuing an item with magic. 
* *Materials*: Iron, steel, mithril, metal from a meteorite, glowing crystals, dragon scales, blood oak, demon hide.

### Grappling Hook

You can pull yourself to a far object, or pull it to you (whichever is more logical). If you or your target is falling, you can use this instantly.

* *Describe*: Launching a grappling hook, throwing a tether, using a web-shooter, firing a magnet gun.

### Identify

When you examine a magic item, the GM must tell you all of its properties. Then, collaborate on 1 additional property that you discover or unlock. 

* *Describe*: Reading an item's aura, recalling its history, noticing tiny details that tell a deeper story.
* *Questions*: How is this blade connected to demons? What happens when this shield touches sunlight? 

### Ingenuity

Your ability to fix, build, and deconstruct exceeds normal limits. Collaborate on what you're capable of doing.

* *Describe*: Tinkering with gears, your expert craftsmanship, intuitively understanding how it works.

### Mobile Fortress

You can spend materials to make a structure mobile. Collaborate on how it moves.

* *Describe*: Steering the ship, activating a gravity reverser, fueling the steam engine, triggering magic runes.
* *Mobility*: Floats on water, hovers a few inches off the ground, flies, rolls, teleports to a visible spot at dawn. 

### Pocket Plane

You have an extradimensional space, which is a miniature universe. Collaborate on its properties. On your turn, you can open or close a portal to it.

* *Describe*: Opening a door in the air, a floating island in space, a featureless room, an empty white nothing.
* *Properties*: Size, temperature, if there's fresh air, appearance, dangers, flaws, how large the portal is.

### Prosthesis

You can create a prosthesis and attach it to someone. Prostheses compensate for physical disabilities and can remove permanent conditions, such as amputations. 

* *Describe*: Attaching it through gruesome surgery, building it in a workshop, collaborating with the recipient.
* *Prostheses*: Steam-powered arm, goggle eyepatch, golem leg, metal skin plates, clockwork heart.

### Tool for the Job

You can instantly create an item, which persists as long as it's in your presence. You have 1 rune to spend as if it's treasure to buy item traits for these conjured items. You can sacrifice 1 treasure to give this trait +1 rune. Conjured items refund their runes when they expire.

* *Describe*: Conjuring a tool out of nothing, creating a sword out of sunlight, making a shield out of ice.
* *Items*: Sword, shield, suit of armor, candle, rope, chalk, water skin, hammer, nails, crowbar, pouch, hand drill.

### X-Ray Vision

You can see through materials. Collaborate on what material you can't see through.

* *Can't See Through*: Metal, wood, magic, stone, organic matter, smooth surfaces, things that are green.
* *Describe*: Donning X-ray goggles, emitting X-ray pulses, peering through a magic gem, your eyes changing color.

## Interaction

### A Thing of Beauty

You can make someone obsessed with a visible item worth at least 1 treasure. They gain an overwhelming desire to possess it, or they focus on it while ignoring their surroundings.

* *Describe*: Swinging a hypnotic pocket watch, showing a glittering gemstone, showing how the gears work.

### BEHOLD!

You can make everyone believe that an item does something specific, even if it doesn't. They cease to believe if they closely inspect the item.

* *Describe*: Using words too big for anyone to understand, grandiose threats, cackling madly. 

### Buyer's Market

In addition to what the GM decides is available at every market, shop, or fence you encounter, you can decide 1 additional thing that's in stock.

* *Describe*: Having a keen eye, knowing the best markets, tracking down merchants, sending order requests.

### Communicator

You can spend materials to create a communicator. Creatures with one of your communicators can communicate with each another.

* *Describe*: Handheld device, resonating sonic crystals, bottled air elementals, gizmos linked by ethereal threads.

### Crazy Enough to Work

When you explain a plan that is even theoretically possible, it becomes perfectly feasible. The GM can't cite logic, physics, or difficulty as reasons to decrease your rolls to enact the plan. Also, you're bolstered to enact your plan. 

* *Describe*: Cackling madly, created absurdly complicated blueprints, using technobabble, showing your math.

### Don't Push that Button!

If you warn somebody not to do something harmless and they do it anyway, tell a free Good Tale about how they made a terrible mistake.

* *Describe*: Using big words to confuse them, implying terrible consequences, acting like you know a secret.

### It's Dangerous to Go Alone, Take This

When you give someone a new or improved item, they're bolstered to use that item.

* *Describe*: Use small words to explain how it works, giving the right tool at the right moment, enchanting it.

### Spark

You can give an item sentience. Collaborate on its personality, and if it can move and communicate. It gains XP equal to half your total XP. Its disposition toward you significantly improves.

* *Describe*: Inscribing a mind rune, enchanting it, giving it a True Name, igniting it with the life spark. 

