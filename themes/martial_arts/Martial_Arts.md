# Martial Arts 

Mind, body, and spirit: these are the three components of self. To find harmony between them is to achieve oneness with the universe. When that happens, all is possible: the body grows as tough as iron and as fluid as the wind; the mind discovers perfect clarity; the soul achieves supreme enlightenment. Meditate on your purpose, young disciple, and behold your infinite potential.

* In a temple nestled high atop a mountain, a monk practices martial arts to achieve enlightenment.

* A nameless warrior wanders the countryside, dispensing ancient wisdom to all those willing to listen.

* A dwarf spends his evenings in the local tavern and wrestles anybody who looks at him the wrong way.

## Combat 

### Catch Blade

Gain the defense "Catch Blade []." Mark it when you suffer a Bad Tale from something small enough that you can catch with your empty hand. If it's a weapon, you can take it from the attacker.  

* *Describe*: Catching a sword between your hands, grabbing an arrow before it strikes you, punching a spell.

### Crane Stance

If you are not yet attacked in combat and someone charges you, prevent their attack and tell a free Good Tale against them.

* *Describe*: Waiting in a defensive stance, giving them a chance to stand down, being passive like the waves.

### Drunken Master

When you're drunk, you're bolstered to fight. If you're extremely drunk, you're bolsteredthree times to fight.

* *Describe*: Wobbling in a confusing rhythm, using the alcohol to fuel your energy, hiccuping, ruddy cheeks. 

### Finisher

You can knock out a creature that isn't significantly more powerful than you. Collaborate on how this trait works.

* *Describe*: Delivering an uppercut, putting them in a sleeper hold, paralyzing them with a special technique.

### Fist of the East Star

If a creature you damaged in the last several minutes dies, you can have it remain alive but incapable of fighting. For the next several hours, you have total control over their biology.

* *Affect their Biology*: They explode, their arm breaks, they fall asleep, they're permanently paralyzed. 
* *Describe*: Hitting a pressure point, striking their sacred chakra points, using a forbidden death technique.

### Iron Grip

Once you grab something, nothing can forcibly break your grip except amputation or defeat.

* *Describe*: Grabbing on with an iron grip, using a wrestling move, putting their limbs in a lock, jumping on.

### Judo

You can make a creature hit itself.

* *Describe*: Turning back their momentum, using their size and recklessness against them, tripping them.

### Pressure Point

When you exploit a creature's weakness, tell a free Good Tale against it. 

* *Describe*: Striking their most vulnerable spots, hitting their chakra points, hurting a nerve cluster.

### Stunning Palm

You can make a melee attack that causes the target to  skip their next turn. Then, they're immune to your Stunning Palm for several minutes.

* *Describe*: Stunning them with a blow to the head, incapacitating them with a kidney shot, locking their chakra.

## Exploration

### Balance

When you experience extreme imbalance, write the imbalance on your character sheet. Cross it off to gain a free Good Tale to balance the scales.

* *Describe*: Pursuing moderation and balance, observing yin and yang, restoring order, seeking true harmony.

### Centered Breath

You can hold your breath for about 1 hour, and you only need about 1 hour of sleep instead of the normal amount. 

* *Describe*: Meditating quietly, controlling your breathing and pulse, benefiting from harsh training.

### Enlightenment

You can ask any question, which the GM must answer. Collaborateon how this trait works. 

* *Describe*: Understanding the inner workings of the universe, achieving true understanding, seeing the truth.

### Leaping Tiger

Collaborate on how much higher and farther you can jump than normal. Also, you can wall jump.

* *Describe*: Making great leaps, gliding through the air, jumping from wall to tall, leaping with the wind.

### Light Feet
Collaborate on how much farther than normal you can fall without suffering harm. All solids and liquids can support your weight.

* *Describe*: Balancing on a single blade of grass, leaping with the grace of a cat, falling like a feather.

### Third Eye

You can perfectly see everything within a far distance, stripped away of all illusion and deception; you see things as they truly are. This functions even if you're blind. 

* *Describe*: Using supernatural senses, seeing things as they truly are, sensing everything within your ki aura.

## Interaction

### Karma

When you perform a sacrificial good act that doesn't benefit you whatsoever, write on your character sheet that you gain 1 Karma (max 3). Spend 1 Karma to tell a free Good Tale.

* *Describe*: Balancing the cosmic scales, achieving harmony, completing a karmic cycle, getting a fair reward.

### Pacifist

Gain the defense "Pacifist []." Mark it when you suffer a Bad Tale while pursuing nonviolence or showing mercy. 

When you do, name an immediate impending threat; it won't come to pass, or it will be much less severe if it does come to pass (GM's choice).

* *Describe*: Extending your hand to an enemy, disarming a foe and leaving them unharmed, seeking peace.

### Sensei

When you give someone in-depth and thoughtful advice, they're bolstered to follow your advice.

* *Describe*: Seeing what they cannot, understanding the bigger picture, telling a fable to convey a lesson.

### Training Montage

You can retrain in about 1 hour instead of 1 week. Others who train with you during this time can also retrain. 

* *Describe*: Working out, sparring, singing Eye of the Chimera, pushing each other beyond your limits.
