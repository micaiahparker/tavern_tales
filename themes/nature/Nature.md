# Nature

Man against nature-it is a popular theme in countless legends and bardic songs. Sometimes the hero can brave the untamed wilderness, but other times the waves swallow his ship whole, or the unforgiving cold of the frigid north drains the warmth from the hero's veins. As civilizations struggle against the ever-encroaching wilderness, the druids and the mystics of the world ask, "Why fight it at all?" These naturalists empower themselves with the vibrant life that courses through massive trees. They master the endless cycle of life and death, ensuring prosperity for their allies and death for their foes. They commune with an ancient, primordial force that has thrived since the dawn of time. What choice will you make? Will you fight for survival against the wilderness? Or will you join with it and harness the power of nature itself?

* A druid defends his forest against hunters and defilers.

* Found at the edge of a forest, a young half-fey possesses unearthly beauty and a deep connection to the wilderness.

* A sentient tree travels the forests of the world in search of others who are like her.

## Combat

### Entangle

You can bind someone to their location until their bindings are destroyed. 

* *Describe*: Growing roots to ensnare its feet, wrapping it in vines, lashing it with a thorny vine-whip.

### Faerie Fire

You can give someone the condition "Faerie Fire" for several hours. They glow and cannot successfully hide, no matter what. Their silhouette is faintly visible through opaque surfaces. 

* *Describe*: Illuminating them in an otherworldly blue glow, surrounding them with buzzing faeries.

### Roots

You can root yourself to the ground. You can't be forcibly moved unless the ground you're rooted to is moved. While rooted, you have the defense "Rooted [][][]." Mark it when you suffer a Bad Tale related to physical harm.

* *Describe*: Joining with the earth, entrenching into the ground, growing roots and vines that grab on.

### Thorns

When someone attacks you skin-to-skin, gain a free Good Tale to harm them. Wearing thick clothes or armor may suppress this effect (GM's discretion).

* *Describe*: Thorns growing out of your skin, your nettle-like skin, poisonous toxins oozing from your pores.

### Undergrowth

You can create a zone for several minutes. Creatures other than you move through it about half as fast as normal. 

* *Describe*: Creating thick underbrush, raising roots to trip feet, growing wooden spikes from the earth.

## Exploration

### Druidic Stones

You can imbue the terrain with one of your traits. Creatures in the terrain have that trait as a 
temporary trait.

* *Describe*: Creating standing stones with glowing runes, attracting magic will-o-wisps, empowering nature.

### Faerie Ring

You can walk in a circle to create a faerie ring. Collaborate on how everything inside the circle is hidden. 

* *Describe*: Casting fey magic, creating a glamour, draping a curtain of light and shadow over the area.
* *Hidden*: Wisps confuse intruders, the contents look like part of nature, a maze-like forest disorients travelers.

Favored Terrain Collaborate on a biome. Gain the defense "_____ Master [][]," with the name of your biome in the blank. Mark it when you suffer a Bad Tale that relates to that biome. Alternatively, you can mark it to increase a roll related to your biome.

* *Biomes*: Underground, plains, forest, urban settings, the open sea, jungle, desert, tundra, swamps, mountains.
* *Describe*: Drawing from past experience, knowing how to use the terrain, attuning to nature around you.

### Fortress Seed

You can create a fortress seed (max 1). Plant it and name a structure. The seed grows into that structure in a few minutes or hours, depending on size. 

* *Describe*: Harnessing nature's power, causing rampant growth, how vines and roots grow explosively.
* *Structure*: Bridge, building, wall, fortress, door, boat, ladder, stairs, tree, fence, wagon, statue.

### Tree Meld

You can move through wood as if it is the consistency of sap. You can breathe normally and sense your surroundings while in wood.

* *Describe*: Turning your skin into wood and vines, melding with the tree, swimming through the wood.

### Verdant Growth

You can reshape 1 space of plant matter, or cause far plants to grow as if they had 100 years of uninterrupted growth. You can cause plants to bear fruit in this way.

* *Describe*: Causing vines to twist and grow, shaping wood like clay, creating an explosion of rampant growth.

### Wild Step

Rough natural terrain such as mud, thick roots, or snow never hinders your movement. Also, you don't leave behind any tracks.

* *Describe*: Stepping lightly, running like beasts of the wild, nature changing to accommodate your steps.

### Will-o-Wisp

You can create a will-o-wisp, which is a very small but eye-catching guide. Name someone and describe something. The wisp will intelligently guide that person to the closest place that matches your description. It has supernatural knowledge and can locate anything that isn't hidden. They fulfill their purpose eternally unless you dismiss them or they're destroyed.

* *Commands*: Bring villagers here, lure bandits to my trap, bring my allies to me, guide me to safety. 
* *Describe*: Conjuring a mote of light, following a bird, calling a pixie to serve you, following a shooting star.

## Interaction

### Fey Beauty

You are supernaturally attractive. Others are significantly more inclined to seek your romantic affection. Collaborate on what this entails.

* *Describe*: Flawless skin, deep eyes, lovely curves, pouty lips, flowing hair, strong jawline, high cheekbones.

### Awaken

You can animate a plant into a living, thinking, mobile creature. Collaborate on their personality. Their disposition toward you significantly improves. 

* *Describe*: Awakening its sleeping soul, accelerating its growth, giving it the primal gift of consciousness.

### Commune

You can communicate with nature guardians. Guardians are powerful spirits that represent the biome where they live. They seek to protect their biome and hold incredible power it.

* *Describe*: Performing a tribal dance, burning incense to awaken the spirit, seeing it manifest under starlight.
* *Guardians*: Silver stag in the tundra, lava beast in a volcano, ancient tree in the forest, water spirit in the sea.
* *Power*: Reshape the land, control local animals, know everything that happens within their biome.

### Fey Curse

You can relinquish power you have over someone to curse them. The more power you relinquish, the stronger the curse. Collaborate on the curse's effects and what lifts the curse. 

* *Curses*: Sleep until they're kissed, they can't leave a forest, they turn to stone at daytime, they lose their voice.
* *Describe*: Saying a magic poem, casting a powerful enchantment, twisting their fate-strands, hexing them.Relinquished 
* *Power*: They break your deal, they grovel at your feet, you hold their most prized possession.

### Fairy Gold

You can make an item (max 1) look like 5 treasure for several hours. 

* *Describe*: Casting a fey glamour, deceiving with an illusion, altering its appearance, enchanting their desires.

### Tree Speaker

You can communicate with trees and other large plants.

* *Describe*: Hearing voices in the creaking wood, drawing forth their tree-soul, speaking through the earthmother.
