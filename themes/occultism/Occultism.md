# Occultism

Lurking in the shadowy corners of the universe are dark entities far beyond our understanding. They gaze upon mortals with equal parts disgust and hatred, patiently plotting ways to plunge the world into darkness and misery. Wise men turn their backs on these dark entities. But for some adventurers, ambition far outweighs wisdom. Power-hungry cultists and warlocks readily embrace these dark patrons, signing infernal contracts to sacrifice their souls in exchange for unnatural power. You could join them... all you need to do is sign a contract in blood. After all, when it comes to fulfilling one's darkest desires, everyone has a price. What is yours?

* A warlock signs a contract with a demon to gain infernal power in exchange for servitude.

* An astronomer gazes at the stars and discovers a dark entity that grants her forbidden knowledge.

* After getting lost in the woods, a girl meets a twisted forest spirit that allows her to join the eternal hunt.

## Combat

### Blood Link

You can link 2 willing creatures, designating one as the giver and the other as the receiver. Any harm the giver suffers is instead transferred to the receiver. The link ends when either person wants it to end. If you link a defeated creature, they don't have to be willing and the link doesn't end when they want it to. Collaborate on what ends it.

* *Describe*: Stitching their souls together, creating a thaumaturgic link, altering the strands of destiny.

### Blood Sacrifice

You can give yourself a condition and bolster to do anything.

* *Describe*: Sacrificing your blood to your patron, flagellating yourself, letting your parasite feed.

### Pound of Flesh

You can give someone an exact copy of one of your conditions. 

* *Describe*: Making them pay blood for blood, evening the cosmic scale, getting bloody revenge, causing recoil.

### Terrify

You can give someone the condition "Terrified." 

* *Describe*:  Showing them their worst nightmare, giving them a glimpse of your patron, using dark magic.

### Vessel

You can mark a creature you're touching (max 1) as your vessel. You can use your traits through your vessel as if you're standing at their location.

* *Describe*: Using blood magic to control its muscles, treating its soul like a puppet, sharing your power.Marks: A tattoo that glows with hellfire, a scar-rune, a glowing symbol floating above their forehead.

### Voodoo

If you destroy or defile something that is important to someone else, gain a free Good Tale to tell against them.

* *Describe*: Creating a voodoo doll, using thaumaturgy magic, corrupting their essence,  your dark ritual.

## Exploration

### Cast into the Void

You can open a 1-space hole to the void for several minutes. The void is in an endless, empty nothing. Anything cast into the void is gone for all eternity.

* *Describe*: Cutting open a bloody wound in the universe, opening a portal, creating a swirling black hole.

### Circle of Binding

You can walk in a circle. When you're finished, create a barrier along the circle. Absolutely nothing can cross the barrier except light and sound. Collaborate on what breaks the circle.

* *Breaks the Circle*: Time passes, an item touches it, you touch the circle, a secret word is spoken, night falls.
* *Describe*: Drawing runes in chalk, lighting candles in a ring, creating a magic circle, using pact magic.

### Conjuration

You can link a creature you're touching with an item you're touching, or to a new item you create. Whoever holds the item can expend the link to instantly summon the linked creature to the item's location.

* *Describe*: Pulling on a soul-tether, opening  a portal to the destination, conjuring it in a puff of smoke and fire.

### Darkness

You can create a zone of pure darkness for about 1 day. No light can enter the zone.

* *Describe*: A swirling black hole, drowning the light, a pulsating black star, shadows spreading like a swarm.

### Defile

You can give the terrain a descriptor outside of what is natural for that terrain. Collaborate on how the terrain gradually shifts to match your descriptor. 

* *Describe*: Spreading magic, spilling your patron's blood on the earth, weakening the barrier between realms.
* *Descriptors*: Hellish, web-strewn, rotten, sacred, verdant, crawling, swampy, barren, nightmarish.

### Portal

You can create a portal (max 5). Anyone who enters one of your portals can emerge at one of your other portals. 

* *Describe*: Creating teleportation stones, building way-points, creating a wormhole, opening a portal.

## Interaction

### Binding Contract

If you like, contracts and agreements you sign or mediate are magically enforced. They can't be violated by any means (Agreeing to the contract "You can't speak of an event" means you literally lose the ability to speak of it forever). The contract or agreement must mention that the terms are binding. 

* *Describe*: Signing a pact in blood, binding their soul to service, write your contract into universal laws.

### Blissful Agony

You can make a creature you're touching experiences any physical sensation you want. This doesn't cause physical harm.

* *Describe*: Bringing them to the brink of ecstasy, inflicting terrible agony, toying with their skin, torture.

### Dark Bargain

Your patrons can perform favors for you, but always at a proportional cost. You have a patron, which is a powerful entity that can grant you power, favors, or knowledge, but always at a cost. Collaborate on your patron. 

* *Cost*: Sacrifice an innocent in their name, start a cult in their honor, further their goals, offer part of your soul.
* *Describe*: Conjuring them in a summoning circle, drawing them forth from the ether, hearing their voices.
* *Patrons*: Demons, angels, otherworldly horrors from beyond the stars, spirits of the land, Death itself.

### Deal with the Devil

After talking to someone, you can learn the one thing that they want more than anything else. If you provide them with that thing, you gain complete and total control over them; the GM or player hands control of that character over to you. 

* *Describe*: Purchasing their soul, making an offer they can't refuse, eternally enslaving them to your will.

### Shatter Mind

You can give someone a psychological condition.

* *Describe*: Ripping their mind apart, shattering their fragile psyche, giving your patron access to their mind.
* *Psychoses*: Schizophrenia, sociopathy, a crippling phobia, paranoid delusions, hallucinations, obsessiveness.

### Soul Gem

You can transform the souls of dead, worthy creatures into treasure (the GM decides how much).

* *Describe*: Coalescing their soul into a black gem, trapping their spirit in a jar, trading souls for favors.
