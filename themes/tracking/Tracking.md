# Tracking

Some look out into the horizon and see only uncertainty and danger. They turn their backs on the unknown and opt for the safety of a simple life. But not trackers. These courageous trailblazers gaze into the wilderness and see a world filled with adventure, opportunities, and wonder. They have climbed the tallest mountain and looked down upon the world like gods. They have plumbed the deepest caves to unearth secrets never intended to see the light of day. They have crossed the widest oceans to discover unknown lands. Trackers' motives are as diverse as the regions they explore. Some hunt elusive game animals. Some seek the bounty of a wanted criminal. A chosen few are simply in it for the thrill of discovery. So, strap on those well-worn boots and grab your traveling pack! What will you discover as you explore the great unknown?

* A relentless bounty hunter tracks wanted fugitives.

* An explorer travels the world in search of ancient artifacts and mysterious treasure.

* From deep behind enemy lines, a scout tracks enemy troop movements and relays the critical information back to her allies.

## Combat

### Black Arrow

You don't need to roll to attack with your very last ammo. If the attack is even theoretically possible, you hit with perfect accuracy and incredible power. Collaborate on what incredible power means.

* *Describe*: Making a desperate shot, putting your hope into your last arrow, drawing a special ammo.
* *Incredible Power*: Knock them back, knock something out of their hand, your projectile pierces through.

### Called Shot

You can make an attack to inflict a condition on an arm, leg, or similar appendage. 

* *Describe*: Aiming for a weak spot, hitting them where it hurts, firing a poisoned arrow, striking true.Conditions: Arrow to the knee, hamstrung, shot through the hand, broken tail, tattered wings.

### Cover

When you're behind cover that you've never used with this trait before, gain the defense "Cover []." Mark it when you suffer a Bad Tale that can be mitigated by that cover. Moving to sufficiently different cover gives you a new defense with an unmarked box. 

* *Describe*: Diving behind a wall, ducking under a bush, crouching behind a cart, hiding in a trench.

### Eagle Eye

Collaborate on how the distance of your ranged attacks exceeds normal limitations.

* *Describe*: Arcing your bow for maximum distance, drawing a bead with your rifle, aiming carefully.
* *New Limits*: A mile, anything you can see, anything under the open sky, several blocks, the moon.

### Favored Enemy

Collaborate on a type of creature. When you spend a Good Tale to harm a creature of that type, gain a second free Good Tale to harm them.

* *Describe*: Training to take down your foe, fighting with pure anger, knowing how to take down that foe.
* *Foes*: Dragons, wizards, beasts, citizens of the Golden Kingdom, elves, undead, vampires, aquatic beasts.

### Set Trap

You can create a well-hidden trap. It triggers when a creature other than you is close enough to touch it. Collaborate on its effects.

* *Describe*: Hiding a trap under leaves, stringing a trip-wire, digging a hidden pit, placing a bear trap.
* *Effects*: Explode, trigger a loud alarm, spew poison, create a pit, catch fire, ensnare them in a net.

### Volley

You can make a ranged attack against 2 creatures.

* *Describe*: Launching a volley of arrows, dual wielding pistols, shooting rapid-fire, spraying bullets.

## Exploration

### Deduce

When you investigate a scene, the GM must describe what happened as if you were there watching it unfold. 

* *Describe*: Reading their tracks to determine what happened, following the trail, deciphering the evidence.

### Know the Path

You can name a visible creature as your quarry (max 1). The GM must answer honestly whenever you ask where your quarry is. They stop being your quarry when they die or you touch them. 

* *Describe*: Reading tracks, following your instincts, using your soul compass, tracking their scent.

### Lay of the Land

You can have the GM create a map of the region and show it to you. 

* *Describe*: Examining the landscape, scanning from a high vantage point, feeling the earth beneath you.

### Reconnaissance

When you extensively observe something, you're bolstered to confront, overcome, or avoid that thing. The longer you observe, the more bolsters you accrue (max 5). You can report your findings to transfer any of these accrued bolsters to others.

* *Describe*: Following enemy troop movements, scouting ahead, patrolling the wilderness for your prey. 

### Survival Skills

You can survive indefinitely off the land, no matter how harsh the conditions. You can also provide for about a dozen other people. When you sleep in the wilderness, treat it as comfortable bed rest with medical attention.

* *Describe*: Identifying native species, living off the land,  relying on instinct, hunting prey, foraging for food.

### Trailblazer

Gain the defense "Trailblazer []." Mark it when you suffer a Bad Tale while exploring. Also, you have a perfect sense of direction. 

* *Describe*: Venturing forth into the great unknown, go where no one has gone before, watching your footing.

### Vigil

You're always considered to be watchful and alert, even while asleep. Also, the GM must answer honestly whenever you ask if you're in impending danger.

* *Describe*: Sleeping with one eye open, watching your surroundings, listening to your instincts.

## Interaction

### Deputy

Collaborate on how you're a representative of the law, and what special privileges and resources this status grants you.

* *Describe*: Flashing a badge, reciting your official title, showing a warrant, leveraging political connections.Resources and Privileges: You can arrest people, local peace keepers help you, some laws don't apply to you.

### Detective

Three times each quest, you can have the GM give you a clue about whatever you're currently investigating.

* *Describe*: Watching their eyes, reading their body language, noticing discrepancies in their stories. 

### Lone Wolf

You can have others generally ignore you as long as you don't draw attention to yourself. Afterward, they forget everything about you, vaguely remembering you as just some person.

* *Describe*: Pulling up your hooded cloak, hanging in the shadows, keeping to yourself, giving the cold shoulder.

### Perks of the Job

When you accept a quest, explain what extra aid you obtain for free. 

* *Describe*: Asking for an advance, attracting aid due to your fame, leveraging your contacts, getting lucky.Aid: Gain part of the reward upfront, learn useful information, attract minions, acquire special tools.

### Skeptic

The GM must answer honestly whenever you ask if someone is lying.

* *Describe*: Watching their eyes, reading their body language, noticing discrepancies in their stories. 

### Trophy

You can collect 1 trophy from each dead worthy creature. These trophies are worth 1 treasure for every 10 XP the creature had. If the creature was especially well known, the trophy is worth twice as much.

* *Describe*: Wearing the trophy around your neck, hanging it from your belt, mounting it on your wall.
* *Trophies*: Bloody head, your enemy's shattered weap-on, tooth necklace, pelt, skull, patch of tattooed skin.
