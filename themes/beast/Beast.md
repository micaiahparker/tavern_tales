
# Beast

Survival of the fittest: in the wilderness, this is the one and only law that matters. There are no morals, no civilized values only the strong and the dead, the predators and the prey. The wilderness is the ultimate crucible, forging beasts over millions of years into perfect killing machines. Their claws are sharper than swords; their scales are thicker than plate armor. What warriors spend decades training to master, predators already know by instinct. Their advanced adaptations gills, wings, and enhanced senses ensure that no prey ever escape their constant hunger. You are the product of countless generations, a finely-honed creature crafted by mother nature herself to do one thing: survive. It is now your turn to survive the trial of blood and fang. Will you be the hunter, or the hunted?

* Given sentience by a druid, an intelligent wolf hunts the human trappers who butchered his pack.

* A shaman draws power from her spirit animal, an ethereal bear that has protected her bloodline for generations.

* A naga, a terrifying hybrid of man and serpent, raids coastal fishing villages to secure bloody sacrifices for his shark-toothed god.

## Combat

### Envelop

You can put a smaller creature inside of you. They can't escape until you're defeated or you let them, and they gain the condition "Crushed" or "Digested" (your choice) while inside of you. However, physical conditions that they inflict on you are much worse than normal.

* *Describe*: Swallowing someone whole, wrapping with tentacles, constricting them with your snake-body.

### Fight or Flight 

Collaborate on what constitutes being close to death. When you're close to death, increase all rolls to fight or flee (choose one) until you're safe. 

* *Describe*: Acting like a cornered beast, running on instinct, becoming feral, doing whatever it takes.

### Fling

You can fling a creature that's smaller or lighter than you a close distance. 

* *Describe*: Knocking them aside with horns, slamming into them, biting them and flinging them into the air.

### Gorgon's Gaze

You can set someone (max 1) under your gaze. While under your gaze, they move half as fast as normal and have the condition "Turning to Stone." 

* *Describe*: Your serpentine eyes, freezing them with your baleful gaze, poisoning their mind and muscles.

### Rend

You can attack someone to give them the condition "Bleeding Out." 

* *Describe*: Going for the jugular, ripping their flesh to shreds, anti-coagulant venom, disemboweling them.

### Trample

You can charge more or less in a straight line, moving through other creature's spaces. Melee attack each creature you move through. 

* *Describe*: Crushing them underfoot, bull rushing past them, turning ghostly and ripping through their souls.

### Tooth and Nail

Give your body 5 item traits. These don't affect the cost of future item traits for your body. 

* *Describe*: Glossy chitin armor, thick scales, serrated teeth, curved talons, a tail with a barbed stinger.

## Exploration

### Burrow

You can slowly burrow through the earth. If you like, leave behind a 1-space tunnel.

* *Describe*: Digging with heavy claws, slithering through soil like a worm, pushing apart rock with your mass.

### Cheetah's Swiftness

Collaborate on how you're much faster than normal, and how quickly traveling like this exhausts you. 

* *Describe*: Sprinting at top speed, running in leaps and bounds, charging across the open plains.

### Enhanced Senses

Collaborate on an enhanced natural sense that you have. 

* *Describe*: Studying others with slitted cat-eyes, sniffing the air or the ground, your ears perking up suddenly.
* *Senses*: Bat-like echolocation, wolf-like sense of smell, cat-like night vision, rabbit-like sense of hearing.

### Kaiju

Collaborate on how you're bigger or smaller than normal. 

* *Describe*: Tiny frame, high-pitched voice, massive size, shaking the earth with each step, booming voice.

### Spin Web

You can produce sticky or non-sticky web strands that are stronger than steel chains. If sticky, they powerfully stick to everything other than you that they touch.

* *Describe*: Spinning a deadly web, dangling strands from the ceiling, platinum strands that glimmer faintly.

### Swarm

You can disperse into a swarm or reconstitute around one of your component pieces. At the GM's discretion, losing parts of your swarm may harm you.

* *Describe*: Falling apart, vanishing in a cloud of creatures, scattering into holes and cracks, dispersing.
* *Swarm of*: Rats, bats, snakes, ants, bugs, leeches and worms, flies, motes of light, woodland creatures, birds.

### Wall Climbing

You can adhere to any solid surface, including ceilings.

* *Describe*: Holding on with powerful claws, adhering with sticky feet, attaching with webbing or sticky goo.

## Interaction

### Awaken Beast

You can give an animal human-like intelligence and the ability to speak. Collaborate on its personality. Its disposition toward you significantly improves.

* *Describe*: Unlocking its mind, giving it a fragment of your consciousness, accelerating its evolution.

### Alpha

When you defeat a beast, you can force it to become your minion with the contract, "_______ will loyally serve you in exchange for indulging its instincts." 

* *Describe*: Holding your teeth to their neck, staring them down, integrating them to your pack, a battle of will.

### Beast Master

You can communicate with animals.

* *Describe*: Growling and grunting, reading each other's minds, understanding the animal's chittering as words.

### Call of the Wild

You can cause all animals in the region to follow a basic instinct of your choice. 

* *Describe*: Unleashing a primal roar, provoking their instinct, starting a stampede, releasing pheromones.
* *Instinct*: Stampede, gather around a certain location, calm down, flee the area, protect this territory. 

### Gift of the Pack

You can give all of your far allies one of your traits (max 1) as a temporary trait for as long as they're in your presence.

* *Describe*: Leading your pack, spreading a hive mentality, having monkey see monkey do, moving as one. 

### Hive Mind

Collaborate on which creatures are in your hive mind. Creatures in a hive mind share consciousness; when one learns something, everyone else in the hive learns it as well.

* *Describe*: Sharing your consciousness, ruling as the hive queen/king, forming a primal bond.

### Play Possum

You can look convincingly dead for as long as you like. 

* *Describe*: Going limp, your skin taking on a ghastly hue, exaggerating your wounds, slowing your heartbeat.

