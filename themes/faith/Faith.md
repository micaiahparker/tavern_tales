# Faith

Some say that you can measure a person by their friends. If that's true, then what can you say of someone who has forged an alliance with the gods themselves? Such is the power of the faithful. These conduits of divine might spread their god's dogma throughout the land, sharing blessings with their brothers and smiting foul heathens with righteous fury. The gods are eager to share their power with devoted followers... but which deity will you champion?

* The orc shaman of a tribe communes with spirits to determine the best time to march to war.

* A cleric prays to a pantheon of gods, requesting that they aid him on his mission.

* A devout paladin leads an inquisition against heathens who would dare defy the will of his god.

## Combat 

### Bless

You can give someone (max 1) the defense "Blessed []" for several hours. They mark it when they suffer a Bad Tale related to harm.

* *Describe*: Praying for divine protection, anointing them with sacred oils, warding them against evil. 

### Blinding Light

You can give someone the condition "Blind" until their vision clears.

* *Describe*: Emanating divine light, your glowing weapon, a shaft of golden light, searing sunlight.

### Crusade

Whenever you fulfill your deity's dogma in a meaningful way, you're bolstered to fight. 

* *Describe*: Finding strength through faith, praying for divine strength, cleansing your sins before a fight.

### Lay on Hands

You can touch someone to heal 1 of their physical or spiritual conditions. Then, the GM tells them how long they're immune to your Lay on Hands (the stronger the condition, the longer the immunity). 

* *Describe*: Your hands glowing with radiant warmth, pouring your deity's energy through your body.

### Martyr

When you're physically harmed, bolster one of your allies to avenge you. When you're physically defeated, bolster all of your allies to avenge you.

* *Describe*: Making a sacrifice for the greater good, inspiring others through your steadfast faith.

### Shield of Faith

Gain the defense "Shield of Faith []." Mark it when you suffer a Bad Tale while furthering your deity's dogma,. Then, gain a free Good Tale related to your deity's domains.

* *Describe*: Earning your deity's protection, standing strong in defense of your faith, creating a light shield.

### Smite

If you attack someone that your deity despises, destroy them outright. If they're more powerful than you, give them a crippling condition instead. 

* *Describe*: Your sword burning with holy fire, your deity filling you with divine wrath, casting them into hell.

### Turn

You can repel far creatures that your deity despises. They can't approach you, but you can approach them.

* *Describe*: Lifting your holy symbol, shining divine light that burns them, asking your deity to protect you.

## Exploration
 
### Consecrate

You can turn a worthy feature of the terrain into a permanent shrine to your deity.

* *Describe*: Anointing the earth with holy oil, a pillar of light illuminating the area, blessing the holy ground.
* *Worthy Features*: Grand church, statue of your deity, heathen's grave, representation of your deity's domain.

### Light

You can illuminate something in bright light, which lasts for as long as you want. Collaborate on something very general and vague that causes the light to change color or glow more brightly. Also, collaborate on what causes the light to fade.

* *Changing Light*: Someone's approaching, you have strayed from the path, there's hidden danger here. 
* *Describe*: Your eyes glowing with divine light, seeing the world as your deity sees it, knowing the one truth.
* *Fade*: Time, the light is exposed to something your deity despises, the sun sets, the light can be washed off. 

### Miracle

When the GM agrees that you significantly advance your deity's dogma, write on your character sheet that you hold your deity's favor. Cross it off to perform a powerful miracle that falls under your deity's domains. 

* *Describe*: Praying to your deity for sacred power, serving as a conduit for divine wrath, doing the impossible.
* *Miracles*: Resurrect the fallen, receive a vision, know the divine truth, banish a heathen to another realm.

### Pilgrimage

When you reach one of your deity's holy sites, you're bolstered three times to do anything.

* *Describe*: Using church contacts, receiving a vision on the path to sanctuary, completing a sacred journey.

### Sanctuary

When you enter one of your deity's holy sites, name 1 threat. Until you leave, that threat cannot enter the holy site or harm anything in the holy site. 

* *Describe*: Praying for blessed sanctuary, activating holy wards, rebuking foul heathens, finding inner peace.

### Scry

You can view and sense distant areas as if you're standing there. Collaborate on how this trait works.

* *Describe*: Receiving a vision, see a distant area in a dream, look down on the area from the heavens.How it works: You must have been there before, the area must embody your deity or one of its domains.

## Interaction
 
### Atonement

You can give a defeated creature a quest, which becomes their foremost goal in life. If they complete it, their disposition toward you significantly improves.

* *Describe*: Promising redemption, showing them the one true path, offering mercy and understanding.

### Chosen One

Fellow worshipers of your deity regard you as the chosen one, a divine representative of your deity. They treat you accordingly and will go out of their way to help you.

* *Describe*: Declaring yourself a prophet, your divine blood, rising to the highest rank in your religion.

### Inquisition

You can force someone to honestly answer 1 question if they're injured, or all of your questions if they're defeated.

* *Describe*: Torturing them, intimidating them, breaking their spirit, turning the screws, punching their wound.

### Judgment

You can sense sin. Collaborate on what that entails. 

* *Describe*: Seeing a dark stain upon their soul, reading your deity's divine ledger, feeling profane corruption.

### Pray

Your deity responds to your prayers (in some form or another). Your deity must reveal their will to you and may also reveal things related to their domains. 

* *Describe*: Hearing your deity's voice, seeing a sign that reveals your deity's will, feeling the right choice.

### Proselytize

You can give someone the immediate urge to fulfill part of your deity's dogma, or convert to your faith (their choice). 

* *Describe*: Preaching, giving a fiery sermon, manipulating their guilt and passion, promising divine rewards.

### Rabble Rouser

You can stir up a crowd to become incredibly passionate, motivated, and extremist about something they care about. Unless stopped, their extremism spreads and grows.

* *Describe*: Appealing to their sense of duty, giving them a righteous cause, obligating them through scripture.

### Zealotry

Add "or fulfilling your deity's dogma" to the end of your minions' contracts.

* *Describe*: Appealing to their sense of duty, giving them a righteous cause, obligating them through scripture.
