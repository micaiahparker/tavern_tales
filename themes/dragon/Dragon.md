# Dragon

There is no creature more legendary, more powerful, more feared, more awe-inspiring than the dragon. These engines of destruction carve paths of terror through the land, annihilating whole armies and burning the countryside. Mortals who dare oppose dragons are reduced to cinders, while those with the wisdom to offer dragons tribute can bask in their draconic patron's glory. Spread your wings and set the sky aflame, mighty dragon! Prove to the world that your power is unmatched!

* An ancient dragon jealously guards a massive pile of gold deep within his mountain home.

* After drinking the blood of a dragon, a young adventurer discovers he has gained the ability to breathe fire.

* A line of humans have glimmering green scales and supernatural strength. According to legend, their bloodline was sired by a dragon who took the form of a man.

## Combat

### A Dragon Never Forgets

After someone wrongs you and gets away with it, write their name on your character sheet. Cross off a name to tell a free Good Tale to get revenge.

* *Describe*: Brooding in your lair, plotting vicious revenge, monologuing, hatred burning in your eyes.

### Carnage

Physical conditions you inflict are much worse than normal. Collaborate on what that means.

* *Describe*: Tearing off an arm, burning their skin, breaking a bone, plucking out an eye, ripping skin to shreds.

### Dragon's Breath

Collaborate on how you charge this trait. Spend the charge to attack all combatants, excluding you.

* *Describe*: Breathing fire, causing an avalanche, roaring so loud their ears bleed, blasting air with your wings.

### Like Crushing Insects

Instead of rolling to overcome a risk on your turn, you can gain a free Good Tale to defeat 10 or fewer minions in whatever horrific fashion you please. 

* *Describe*: Crushing them underfoot, ripping out their guts, reducing them to ash, flinging them away.

### Scales Like Tenfold Shields

Gain the defense "Scales Like Tenfold Shields []." Mark it when you suffer a Bad Tale from a weapon. When you do, that weapon shatters, breaks, or is otherwise rendered temporarily unusable. 

* *Describe*: A sword shattering on your hide, knocking away a hammer, igniting a bow in dragonfire. 

### Stoke the Furnace

You can gather power. When you tell a Good Tale, you can release all of your gathered power to make your Tale more powerful. Collaborate on what that means.

* *Describe*: Fire burning in your throat, glowing eyes, the ground shakes, electricity sparks around you.
* *Power*: An explosion is bigger, break down a wall instead of a door, shoot a spell much farther than normal.

## Exploration

### Gem-Encrusted Hide

You can destroy a magic item to make it permanently part of you, as if you are now the magic item. When you're slain, the items (or at least the parts necessary to reforge them) can be recovered from your corpse.

* *Describe*: Magic coursing through your veins, feeding on raw energy, storing treasure inside of your body.

### Lair

When you sleep in a place that you control, you can designate it as your lair. Gain the defense "Lair [][]." Mark it when you suffer a Bad Tale while in your lair. Alternatively, mark a box to increase a roll related to your lair.

* *Describe*: Shaping the world in your dreams, imposing your will on the terrain, attracting wildlife like you.
* *Lairs*: Volcano filled with magma pools, sub-zero glacier, humid jungle, fetid swamp, maze-like tunnels.

### My Precious

You know the exact location of items worth at least 1 treasure that you've touched before. Also, the GM must answer honestly whenever you ask how much something is worth.

* *Describe*: Feeling the pull of gold in your blood, your gold-colored eyes, covetously hoarding treasures.

### Smokescreen

You can fill all far spaces with thick, obscuring smoke or fog. 

* *Describe*: Stoking the flames, breathing smoke and ash, boiling water into steam with your fire breath. 

## Interaction

### Pride

Gain the defense "Pride []." Mark it when you suffer a Bad Tale related to damaged reputation or social status. 

Then, gain a free Good Tale to improve your reputation or social status.

* *Describe*: Looking down at your inferiors, your regal bearing, building a fearsome reputation. 

### Serve or DIE!

Whenever you torture or kill one of your minions, your other minions who watched you change the end of their contract to "in exchange for not being tortured or killed." 

* *Describe*: Devouring a weak link, crushing them like an ant, finding their lack of loyalty disturbing.

### Terrifying Glory

You can make everyone stop what they're doing and give you their undivided attention, even if for just a few moments. 

* *Describe*: Bellowing a command, doing something terrifying, showcasing your incredible grandeur. 

### Tribute

After you showcase your incredible power, name a person or organization who knows what you did. They freely offer you a tribute.

* *Describe*: Warning that they're next if they don't pay you, terrifying peasants, showcasing your grandeur.
* *Tributes*: A seat of honor at a major event, treasure, minions, land, a promise not to enter your territory.

### You Are All Beneath Me

Whenever you want, unimportant NPCs instinctively treat you like royalty, showing you incredible respect or terror. 

* *Describe*: Looking down at them, intimidating them with your power, threatening their pathetic lives.

