# Warfare

Warriors make history. They're the ones who slay dragons, who dethrone kings, who stand tall among the brutal carnage of a blood-soaked battlefield and raise their swords in triumph. Some of these men-at-arms have humble origins, taking up weapons in defense of home and family. Others pursue bloodshed as if they are born for it, traveling the land to train under exotic blademasters and master tacticians. Regardless of their past, all warriors follow the one universal truth of combat: kill your opponent before he kills you. And they're damned good at it. Every veteran warrior carries remnants of his past triumphs-each scar a reminder of a wound that could have been his last, each notch in his sword a lesson learned in the importance of precise swordplay. Even now, countless warriors are training for the day when they meet you in battle. Will you be strong enough to vanquish them? Take up arms, warrior, and prove to the world that today is not your day to die!

* The captain of the town guard leads his troops in the defense of an isolated community.

* A master swordsman travels the land in search of a worthy opponent.

* After wining his freedom in a gladiatorial slave arena, a fierce warrior begins his life as a mercenary.

## Combat

### Disarm

You can disarm someone. They drop a held item, which lands in a close space. If you have a free hand, you can catch the item instead.

* *Describe*: Attacking in a flurry, cutting through foes, spinning in a blade tornado, releasing an explosion.

### Every Scar a Lesson

Whenever you're defeated by a worthy threat, gain the defense "Scarred by _____ []." Write the threat in the blank. Mark it when you suffer a Bad Tale related to that threat.

* *Describe*: Jagged cut, burned skin, crooked finger, missing tooth, glowing rune-scar, dented armor.
* *Threats*: Dragon, bug, beast, sword, trap, soldier from the Southern Kingdom, wizard, rogue, frost, fire, acid.

### Guardian

You can intercept attacks if you're in a position where you can logically do so. 

* *Describe*: Interposing yourself, diving in the way, lifting your shield over someone, shoving them behind you.

### Kensai

Give one of your weapons a name. It is now a magic item, but only in your hands. Collaborate on its effects. Absolutely nothing can forcibly remove it from you. If lost, it miraculously finds its way back to you in less than a day.

* *Describe*: Performing a sacred blade ritual, carving the weapon's name into it, baptizing the weapon in blood.Miraculous Return: Trip over it in the woods, find it on the next corpse you loot, find it for sale in a shop.

### Shove

You can push someone back as far as you can normally move. Move into the spaces they vacate.

* *Describe*: A powerful bull rush, lowering your shield and charging forward, making them dodge backward.

### Tough as Nails

Gain the defense "Tough [][]." Mark it when you suffer a Bad Tale that you can ignore with sheer toughness. 

* *Describe*: Scarred skin, thick muscles, shining armor, good old-fashioned grit, raw willpower, magic tattoos.

## Exploration

### Arena

When you set foot on a battlefield, tell the GM about a strategic asset on that battlefield. 

* *Describe*: Examining the field with a tactical eye, noticing an important detail, exploiting the terrain.
* *Assets*: A platform that will crumble from a good hit, a hidden trap, ample cover, a bottleneck.

### Grit

Your endurance exceeds normal limits. Collaborate on what this entails.

* *Describe*: Setting your jaw and powering through, shrugging it off, ignoring pain and aching muscles.
* *Capabilities*: Run a marathon, carry an unconscious ally for hours, ignore your biological needs for a week.

### Guard Duty

While you're paroling, guarding, or escorting, you have a basic understanding of everything that happens in and around whatever you're protecting. Also, you can completely negate 1 Bad Tale against whatever you're guarding during this time.

* *Describe*: Keeping an eye out, watching the shadows, establishing lookouts, guarding the area, setting patrols.

### High Ground

When you reach an area's most tactically advantageous position, immediately tell a free Good Tale. 

* *Describe*: Using the high ground to cut down foes, getting a better vantage point, controlling a bottle neck.

### Strong Back

You have +5 inventory slots. 

* *Describe*: A huge backpack, strong muscles, broad shoulders, bearing the burden, wearing heavy armor.

### Titan's Strength

Your ability to lift and carry heavy weights exceeds normal limits. Collaborate on what this entails.

* *Describe*: Your veins pulsing, straining your muscles, gritting your teeth, heaving something over your head.

### Veteran

You can have the GM tell you what dangers to expect. Explain how you know this information.

* *Describe*: Knowing what to do from past experiences, noticing a tell-tale giveaway, keeping a watchful eye.

## Interaction

### Blood on the Wind

Whenever someone important to you is in danger, you know their exact location and what sort of danger they're in.

* *Describe*: Feeling it in your gut, the hairs on the back of your neck standing up, getting a dark premonition.

### Bushido

When you show an enemy profound mercy or respect, their disposition toward you significantly improves. Also, write on your character sheet that you hold their debt. Cross it off to have them pay off that debt.

* *Describe*: Earning karma, holding others to a high standard, bringing out the best in others.

### Cold Read

You can have the GM tell you all of a visible creature's traits. 

* *Describe*: Reading their body language, getting a gut feeling, making guesses based on their equipment.

### Enemy of My Enemy

When you harm, hinder, or disrupt an enemy, you can have the GM tell you of one of your enemy's enemies. Their disposition toward you significantly improves. Explain how you know this information.

* *Describe*: Teaming up, forging an alliance, putting petty grudges aside, using each other towards your goals.

### Get Their Attention

You can make everyone present think that you are by far the biggest threat until they see evidence to the contrary.

* *Describe*: Performing a deadly weapon maneuver, looking deadly and intimidating, pissing them off.

### Gladiator

After you win a fight, choose someone who watched you fight. Tell the GM how they henceforth feel about you. 

* *Describe*: Showboating, an incredible display of power, pleasing the crowd, flexing your muscles.
* *How they Feel*: Don't cross them, they're so sexy, I need to befriend them, I never want to fight them.

### Iron Sharpens Iron

When you train with others, you can gain 1 of your training partner's traits (max 1) as a temporary trait for about 1 day. Each of your training partners can similarly gain 1 of your traits (max 1) as a temporary trait for about 1 day. 

* *Describe*: Practicing weapon drills, trading war stories, forging spiritual bonds, swearing oaths of loyalty.

### No One Left Behind

You can save someone from any threat imaginable. However, your fates are now intertwined until both of you are completely safe and have no conditions. If one of you gains a condition or dies while your fates are intertwined, the other does as well. You can't use this trait on someone if your fates are already intertwined.

* *Describe*: Leaping to their rescue, dragging your ally to safety, grabbing them as they fall over a cliff ledge.

### War Stories

After you defeat a worthy foe or accomplish a worthy deed, write it on your character sheet. Cross it off to share the story with others. Their disposition toward you significantly improves, or they're intimidated by you and back down (their choice). 

* *Describe*: Telling them your body count, explaining what true danger really is, telling tales in a tavern.

