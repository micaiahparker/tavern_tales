
# Elements

Is there any force in the world more powerful than the primal elements? Earthen mountains reach into the clouds, humbling even the tallest manmade tower. Air creates wind-tossed thunderstorms that steal the warmth from the living. Water is the origin and sustainer of all life, so much so that it flows through our veins. Fire consumes all, bringing complete destruction to anything it touches. Look inside yourself, brave elementalist. Which elements rage within your soul?

* A frost witch lives in solitude in her ice castle and freezes any who dare approach.

* Born of the earth itself, a tough explorer delves into the earth's deepest caves for exotic gems.

* A raging storm gains sentience, coalescing into human form with piercing blue eyes and cloud-white skin.

## Combat

### Affinity
Collaborate on a threat you're completely immune to, and another threat that you're especially vulnerable to.

* *Describe*: Immunity to flames but weakness to frost, immunity to magic but weakness to physical harm.

### Blaze

You can create a damaging zone for several minutes. 

* *Describe*: Setting the ground ablaze, growing razor-sharp ice shards, generating a lightning field.

### Cataclysm

You can start a natural disaster. Once started, the disaster is out of your control (but you might be able to influence it). 

* *Cataclysms*: Tornado, hurricane, earthquake, blizzard, heat wave, dust storm, thunder storm, gale force winds.
* *Describe*: Unleashing raw elemental power, shattering nature's balance, turning your emotions into magic.

### Frostbite

You can freeze less than 1 space of material in ice.

* *Describe*: Locking them in ice, freezing their blood, covering them with frost, surrounding them in earth.

### Immolate

You can give something the condition "On Fire." 

* *Describe*: Igniting their clothes, setting their soul ablaze, surrounding them in blue fire, elemental havoc. 

### Electrocute

You can paralyze someone for a second or two.

* *Describe*: Electricity arcing from person to person, throwing a bolt of lightning like a javelin.

### Path of Fire

You can leave behind a trail of harmful elemental fury in all spaces that you vacate. The trail lasts a few minutes.

* *Describe*: Leaving fire in your wake, creating a deadly static field, raising earth spikes, creating frost shards.

## Exploration 

### Control Weather

You can change the weather to anything that the region could reasonably experience this time of year. The weather changes in a few minutes or hours (GM's choice).

* *Describe*: Changing the air pressure, creating moisture, bringing in a cold front, shaping the clouds.

### Clutch of Earth

You can change the effects of gravity in the area.

* *Describe*: Changing magnetic fields, altering earth's density, enhancing the pull of gravity. 

### Earth Walk

You can move through stone, metal, and earth as if they have the consistency of mud. You can breathe normally and sense your surroundings while in these materials.

* *Describe*: Changing magnetic fields, altering earth's density, enhancing the pull of gravity. 

### Flight

You can fly. Collaborate on how this trait works.
* *Describe*: Sprouting wings, running across the air as if it were solid, riding on powerful winds.How it works: You need safe take-off and landing zones, it's exhausting, you can carry a few others. 

### Iron Body

You weigh up to 20 times more than normal and can't be forcibly moved.

* *Describe*: Rocky skin, iron-hard bones, the gravity of earth pulling you, a body made of stone and crystals.

### Shape Element

Collaborate on an element. You can create 1 space of that element, or manipulate several spaces of that element.

* *Describe*: Practicing element-bending, extending your hand and commanding the elements to respond.

### Terraform

You can reshape the landscape. The terrain changes over the course of days or weeks (GM's choice).

* *Describe*: Growing a mountain, diverting a river, burrowing a cave, opening a chasm, flooding lowlands.

### Tremors

You can sense the movements of things touching the earth within about 1 hour's travel in all direction. Also, you can put your ear to the ground to hear the vibrations of very distant things.

* *Describe*: Feeling the earth tremble beneath your feet, sensing subtle vibrations, hearing the earth rumbling.

### Water Breathing

You can breathe water and ignore extreme pressures associated with deep water. 

* *Describe*: Growing gills, infusing your body with primal water, creating an air bubble around your head.

## Interaction 

### Calm the Storm

You can make others become significantly more passive, calm, and reasonable. This ends if someone acts threateningly toward them.

* *Describe*: Quenching the fires that rage in their hearts, creating a soothing breeze, soothing their inner turmoil.

### Fan the Flames

You can significantly intensify whatever emotions creatures are currently feeling. 

* *Describe*: Adding fuel to their passions, igniting their souls with elemental energy, sparking their desires.

### Port in the Storm

You can find a safe haven that will safely harbor you and your allies for as long as you stay there. The locals will freely offer some sort of aid, or the aid will already be there if the safe haven is unpopulated. 

* *Aid*: Food and shelter, fresh weapons or armor, information, guides, minions, medical attention, potions. 
* *Describe*: Sensing a safe area, feeling the air is calmer over there, following a soothing wind, following warmth.Safe Havens: Hidden cave, a house owned by friends, politically neutral ground, forgotten cabin in the woods.

### Still Air

You can give something the condition "Silenced" for 
several hours. Silenced things can't make any noise whatsoever. 

* *Describe*: Freezing air in place, absorbing sound waves with the earth, keeping the air perfectly still.

### Words on Wind

You can name someone and speak a few sentences. They will hear your message after a few moments. Then, they can speak a few sentences; their message returns to you in a similar fashion.

* *Describe*: Projecting your voice through the air, sending a message to the four winds, exhaling an air elemental.
