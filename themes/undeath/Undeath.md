# Undeath

For some, death is only the beginning. These undead push aside the dirt and slowly climb from their dark graves as flesh peels from their bones. Their tortured souls materialize and hover in the air, as if a cold, blue flame suddenly ignited. They awake on the tables of cruel necromancers and feel an unnatural hunger twist their stomachs. Undead wander the land with a single-minded purpose, incessantly searching to fulfill some dark desire. What is it that ripped you back from the blissful rest of afterlife? Was it unfinished business? Dark magic? An eternal hunger for human flesh? Whatever drives you, the living shall have no respite. Death has come for them.

* A necromancer reanimates the corpses of her enemies to do her bidding.

* A death knight serves his dark god with a blade that drains the energy from his foes.

* Awakened as if from a dream, an undead gains sentience and sets out on a journey to discover what killed him.

## Combat

### Dead Nerves

Gain the defense "Dead Nerves []." Mark it when you suffer a Bad Tale that an undead creature could ignore. If that Bad Tale gave you a condition, completely ignore all negative effects associated with that condition.

* *Describe*: Ignoring a fatal wound, unnatural resilience, undead anatomy, your rotten organs.
* *Ignore*: Your guts spilling out, carry around your decapitated head, ignore a sword through your heart. 

### Death Throes
 Collaborate on something powerful that happens when you die. 

* *Describe*: Your soul exploding, uttering a final curse, completing your most powerful spell, finding vengeance.
* *What Happens*: Explode in necrotic energy, take your attacker with you, return as a ghost, curse someone.

### Death Watch

Whenever you or one of your closest allies would die, they instead hold onto life for several minutes or hours, depending on the severity of their injuries. During this time, they can be healed as normal to potentially avoid 
death. Also, the GM must answer honestly whenever you ask how close a creature is to death. 

* *Describe*: Hiding them from Death, altering their anatomy, extending their life with necromancy.

### Drain Soul

You can take a dying or recently dead creature's soul. Write it on your character sheet (max 5). Others can't interact with the soul (such as to resurrect it or communicate with it) without your consent. Also, you can forever obliterate the soul to temporarily gain some of its power. Collaborate on what that entails.

* *Describe*: Soul strands rising to your clenched fist, capturing its spirit in a gem, devouring its essence.
* *Power*: Increase one of your stats, bolster to do what it did well, gain a temporary trait, summon its spirit.

### Plaguebearer

Collaborate on a disease that you carry, and how it spreads. You're immune to it. 

* *Describe*: Pallid skin, sunken eyes, pus-filled boils on your skin, black blood, skin covered in a sheen of sweat.
* *Plague*: Virus, bacteria, worm parasites, fungal mushroom spores, dark curse, the seed of a corrupted plant.
* *Symptoms*: Vomit and nausea, painful boils, extreme fever, necrosis, coughing, weakness, eventual death.
* *Transmitted by*: Skin-to-skin contact, damaging someone, bodily fluids, drawing a rune on their skin.

### Reanimate

Collaborate on a death condition and how quickly you reanimate. If you die and your death condition isn't met, you return to life.

* *Death Condition*: Staked in the heart, decapitated, killed when your phylactery is destroyed, poisoned.
* *Describe*: Crawling out of a shallow grave, coalescing from death, pulling yourself out of the spirit realm.

### Touch of Death

You can kill a creature that isn't significantly more powerful than you. Collaborate on how this trait works.

* *Describe*: Ripping apart their soul, turning their blood to acid, decapitating them, withering their flesh.
* *How it Works*: Lose a fraction of your soul to fuel the spell, they haunt you afterward, you anger Death itself.

## Exploration

### Bloodseeker

You can hear far heartbeats with perfect accuracy. While focusing on someone, you can sense and understand every part of their physiology as if you'd dissected them and carefully studied every body part.

* *Describe*: Hearing blood course through their veins, noticing subtle changes in their physiology. 
* *Sense*: Smell disease, hear a nervous heartbeat, feel the warmth of arousal, see pregnancy, taste their fear.

### Despoiler

You can cause far plants and objects to wither and decay.

* *Describe*: Withering plants to dry husks, rusting metal, spoiling food and water, warping wood, cracking stone.

### Eternal Hunger

You no longer need to eat or drink. Collaborate on 1 new need that you must fulfill at least once a week or starve. When you excessively indulge this need, you heal as if you received 1 day of bed rest and medical attention, and you're bolstered to do anything.

* *Describe*: Cold skin, no pulse, you don't bleed when you're cut, obsessive desire, growling stomach.
* *New Needs*: Drink blood, eat nightmares, consume souls, feast on fear, absorb magic, devour nightmares.

### Haunt

You can awaken the terrain. It becomes sentient and gains XP equal to half of your total XP, which it can spend however it wants. Collaborate on its personality. Its disposition toward you significantly improves.

* *Describe*: Spilling your blood into the earth, binding a spirit to the land, corrupting the area, enchanting it.
* *Terrain Actions*: Open and close doors, attract or repel creatures, alter its geography, a ship sails itself.

### Pierce the Veil

You can see the spirit world. Collaborate on what that entails.

* *Describe*: Looking through ghost eyes, shifting your soul, your eyes going black, seeing dead people.
* *Entails*: See recently dead spirits, murderers appear to have blood-stained hands, see where a death occurred.

### Unlife and Unlimb

You can attach and reattach your body parts. You control your detached body parts and sense the world through them. You can instantly heal some conditions (such as an amputated arm) in this way.

* *Describe*: Watching through an eyeball, crawling with a hand, pulling your guts back inside yourself.

### Wraith

You can become ethereal or return to your normal form. Ethereal things pass through non-ethereal things like a ghost, and vice versa.

* *Describe*: Taking a ghostly form, crossing dimensions, turning to mist, becoming a living shadow.

## Interaction

### Aura of Death

You can give all far creatures the overwhelming desire to leave the area.

* *Describe*: Whispering dark words, creating a sudden chill, giving people an unsettling feeling in their gut.

### Banshee's Wail

You can emit an incredibly loud sound. Far creatures gain the condition "Deaf" until the ringing tops. The sound also shatters glass and shakes structures.

* *Describe*: Screaming the cries of the dead, wailing in agony, speaking with death's otherworldly voice.

### Braaaiins

You can gain some of a dead creature's knowledge and memories.

* *Describe*: Eating its brains, consuming its soul, enslaving it in the afterlife, absorbing its brain waves.

### Charon's Toll

You can sacrifice 1 treasure to negate 1 Bad Tale.

* *Describe*: Burying coins to pay Death's toll, turning treasure into ash, giving coins to a shadowy hand.

### Medium

You can speak to the dead. You must have access to part of a creature's body, or something that was important to it in life. 

* *Describe*: Opening a door to the afterlife, tugging at a soul strand, sending your voice into the grave world.

### Possess

You can vanish and take control of a corpse or vulnerable creature. Use its stats and traits instead of your own, but you retain this trait. If the body you're possessing has more XP than you, gain a condition you can't remove until you end the possession. Your host can attempt to rebel against your possession. 

* *Describe*: Overpowering its soul, becoming a spirit and moving inside of it, infecting its thoughts.

### Revenant

You can return a dead creature to life. Give them a quest. They remain alive as long as they treat that quest as their foremost goal in life. Collaborate on what happens after they complete their quest. 

* *Describe*: Giving them a second chance, letting them tie up loose ends, ripping them out of the afterlife.
* *After the Quest*: They can do as they please, they can pursue 1 quest of their own choosing before dying.
