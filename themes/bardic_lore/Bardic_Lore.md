# Bardic Lore 

Truly, the pen is mightier than the sword. Just think of how many people have been sent to their deaths at the stroke of a pen, how many marriages have crumbled because of a romantic poem, and how many plans have been ruined due to a clever lie. Those who pursue the subtle arts of bardic lore understand the true value of people, wielding inspiration like a blade and intrigue like a shield. With a pen in your hand, a sword on your belt, and a song (or perhaps a maiden) on your lips, how will you write your own legend? 

* A witty jester entertains the king with his jokes, all while subtly influencing the wealthy elite.

* A bright-eyed bard travels the land, learning new legends and singing songs of lost heroes.

* With his rapier and his feathered cap, a dashing swashbuckler is a danger to seedy criminals and lovelorn maidens... but for very different reasons.


## Combat

### Boost Morale

When you rest for the night, you can bolster each of your present allies to do something during the next day. Decide immediately what they're bolstered to do. 

* *Describe*: Exaggerating the day's adventures, cracking jokes, playing tunes, telling a story, keeping spirits high.

### Duel

You can challenge someone to a duel. Others can't interfere with your duel until one of you wins the duel.

* *Describe*: Telling them to prepare to die, challenging them, clashing swords and grinning at each other.

### Feint

You can make someone hesitate, flinch, or suffer a moment of doubt. 

* *Describe*: Surprising them with impressive swordplay, insulting their mother, upstaging them, mocking them.

### Flair for the Dramatic

Whenever things take a turn for the worse and you're in serious danger, you're bolstered. 

* *Describe*: Spitting out blood and laughing, showing your fighting spirit, improvising, a last-ditch effort.Serious Danger: Some of your allies have been defeated, you're falling to your doom, you're surrounded.

### Inspire

You can bolster an ally to fight. 

* *Describe*: Giving a riveting speech, encouraging others, performing, singing a tune, playing an instrument.

### Raise Spirits

Alcoholic drinks heal you as if they're healing potions (refer to the Alchemy theme), except that they give you the condition "Drunk" instead of "Hungry and tired." 

* *Describe*: Drinking to dull the pain, celebrating with a cold one, chugging, being a typical adventurer.
* *Drinks*: A bottle of wine, a massive stein of ale, a flask of liquor, a line of shots, a few mixed drinks.

### Steal the Spotlight

Collaborate on how you charge this trait. Spend the charge to steal the GM's Bad Tale and turn it into a Good Tale.

* *Charge*: Do something unnecessarily risky, gain a crowd's attention, steal all the credit, gain a reputation.
* *Describe*: Shouting over them, doing something dramatic, countering them in a spectacular display.

### Swashbuckler

Gain the defense "Swashbuckler [][]." Mark it when you suffer a Bad Tale while taking unnecessary risks.

* *Describe*: Flair and panache, leaping head-first into danger, your devil-may-care attitude, laughing.
* *Unnecessary Risks*: Swinging over a lava pit, fighting on a ledge, insulting a deity, trying to ride a dragon.

## Exploration

### According to Legend

When you start a quest or enter a new region, tell the GM that according to legend, something powerful exists in the area. The GM will give you a cryptic clue about where to find it. If you find the source of the clue, you locate the legendary power.

* *Clue*: Where two trees intertwine, the deepest and darkest part of the cave, surrounded by a thousand corpses. 
* *Describe*: Sharing a story you heard, mentioning a rumor, putting together clues from history books.
* *Power*: Lost treasure, magic shrine, portal to another world, underground dungeon, unhatched dragon eggs.

### Comedy and Tragedy

Show your gaming group a "Comedy and Tragedy" token. At any point, give it to the GM to tell a free Good Tale; take it from the GM to have the GM tell a free Bad Tale. Start each session with the token in your possession.

* *Describe*: How your fortune turns, an exciting plot twist, a shift in tone, poetic justice, how fate intervenes.

### Dramatic Entry

When you make a dramatic, flashy, or dangerous entrance you're bolstered to do anything. If a lot of people watched your entrance, you're bolstered again.

* *Describe*: Swinging in on a rope, walking out of an explosion, riding in on a unicorn, kicking down the doors.

### Jack-of-All-Trades

You can gain any trait as a temporary trait (max 1). Collaborate on how that trait is flawed.

* *Describe*: Pulling an ace out of your sleeve, getting beginner's luck, improvising, seeing your training pay off.
* *Flaws*: It's imprecise and messy, it's exhausting, it's less potent, you need a special tool, it's loud and obvious.

### Story Teller

While investigating something, you can announce that you heard an interesting story about it. Everyone at the gaming table quickly decides how much time they want to devote to an interlude (refer to Chapter 6). GM an interlude. Whatever happens in the interlude is what actually happened with whatever you were investigating.

* *Describe*: Telling a story around a campfire, recalling a local legend, telling your friends a tale at a tavern.

## Interaction

### A Night to Forget

Whenever you engage in drunken revelry, you can announce it's a night to forget. The GM tells you where you wake up and gives you 1 condition. Write on your character sheet that you have 1 fuzzy memory (max 5). Cross off a fuzzy memory to suddenly remember what you did that night, and how your actions that night somehow benefit your current situation.

* *Describe*: Buying drinks for everyone, going bar hopping, visiting the brothel, throwing a wild party. 
* *Benefits*: You've been here before, you seduced the evil villain, there's a magic item in your pocket.

### Cameo

When you earn an NPC's respect and good will, write their name on your character sheet. Cross of their name to have them show up. 

* *Describe*: Bumping into them randomly, them saving you in the knick of time, spotting them in a crowd.

### Fame and Infamy 

Collaborate on a reputation for yourself (it doesn't have 

to be true). Whenever you like, others know your reputation, believe at least part of it, and act accordingly.

* *Describe*: Showing a signature scar or tattoo, giving your full name, saying "Don't you know who I am?"
* *Reputations*: Assassin who never fails, magically cursed, the land's greatest duelist, a legend in the bedroom.

### First Impressions

When you first meet someone, the GM must answer 1 question about them.

* *Describe*: Reading their body language, going with your instincts, getting a general vibe, noticing a detail.

### Honeyed Words

You are far more charming and convincing. You can sweet-talk others to do things that they would normally refuse. Collaborate on how your charm exceed normal limitations.

* *Describe*: Giving an impassioned speech, calling in a favor, subtly manipulating someone, being charming.

### Rumors

At any point, you can have the GM tell you 2 facts and 1 lie about whatever you're investigating.

* *Describe*: Overhearing gossip, trading for information, remembering rumors you heard while in town.

### Small World

You can recognize an NPC as an old acquaintance. Explain how you know each other and how they feel about you.

* *Describe*: Recognizing your old stomping grounds, looking up an address, hearing so-and-so is in town.
* *Old Acquaintances*: Drinking buddy, cousin, former adventuring partner, old flame, business partner.

### Wingman

When you enter someone's good graces, bolster an ally to socialize with that person.

* *Describe*: Chatting up your friend, telling them what to say, lightening the mood, making introductions. 

### Villainous Monologue

You can make someone start rambling about whatever's important to them until they reveal too much.

* *Describe*: Letting them think they've won, buying them a drink, shouting "You'll never get away with this!" 



