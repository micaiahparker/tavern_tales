# Command 

Who is it the most powerful person alive? Is it the mighty swordsman who can defeat any other foe in single combat? Is it the clever wizard who has mastered thousands of spells? Is it the crafty rogue who can go anywhere unnoticed? No-the most powerful man alive is whoever has the biggest army. Commanders and kings understand that power doesn't lie in trivial things like swordplay or magic. It lies in people. It lies in loyalty. It lies in the ability to issue a single command and watch as your army burns your enemy's kingdom to the ground. You can wield such power. By mastering diplomacy and statecraft, you can amass an army like the world has never seen. Rally an army to your side, lord commander, and lead your people to war!

* A necromancer hides within a catacomb, biding his time while he amasses an unstoppable skeletal army.

* With a riotous crowd of peasants behind him, a fiery-eyed visionary leads a revolt against an oppressive regime.

* A decorated commander oversees the fortification and defense of an invaluable fortress.

## Combat

### Abandon All Hope

When you fight enemies who have seen warnings of your power or ferocity, gain 1-3 Good Tales to show them why they should have heeded your warnings. 
 1. Ominous (war drums in the distance)
 2. Obvious (you decapitate their emissary)
 3. Horrific (a pile of corpses with your flag at the top)

* *Describe*: Crushing their hope, spreading fear and menace, emboldening your troops while terrifying theirs.
* *Warnings*: Roar in the distance, war drums, haka dance, pile of corpses, decapitated heads on pikes.

### Banner

You can baptize an item worth at least 1 treasure in the blood of worthy foes (literally or metaphorically). The item becomes your banner (max 1). You and allies each gain the defense "Under a Banner []." Mark it when you suffer a Bad Tale related to battle while the banner is visible. If it's lost or destroyed, you and allies gain the condition "Demoralized" until it's replaced. Banners are inherently flashy and attract enemies' attention.

* *Banner*: Flag, coat of arms, shield with an emblazoned symbol, glowing arcane symbol, skull on a pike. 
* *Describe*: Waving a banner, getting inspiration from a greater cause, holding it above the battlefield.

### Give Order

You can give your ally an order and your unspent Good Tale. They resolve the Good Tale instead of you, but only if they use the Good Tale to follow your order. 

* *Describe*: Shouting a command, warning someone of incoming danger, call for a flanking maneuver.

### Shake it Off

You can make someone ignore all conditions that they can logically ignore for about 1 hour. Then, draw a symbol next to those conditions. Those conditions can't be affected by your Shake it Off again.

* *Describe*: Inspiring them to press on, yelling at them until they stand back up, getting their adrenaline going.

### Victory Rush

When you win a fight or overcome a challenge, bolster yourself or an ally to do something of your choice.

* *Describe*: Feeling alive, reveling in your triumphs, standing over your fallen foes, sharing in glory.

### War Cry

When you enter combat, bolster one of your allies to fight. 

* *Describe*: Giving a riveting speech, throwing back your head and giving a war cry, shouting "CHARGE!"

## Exploration

### Fortified

Gain the defense "Fortified [][][]." Mark it when you suffer a Bad Tale while in a fortified location you control. The maximum number of boxes you can mark depends on your fortifications:

* [] Light fortifications (boarded cabin, bottleneck)

* [][] Strong fortifications (keep, cave)

* [][][] Heavy fortifications (castle, fortress)

* *Describe*: Invading neighboring territory, claiming land for yourself, taking the spoils of war, stealing resources. 

### Just as Planned

You can write down an event that's out of your control. Seal it in a document with a code name on it and give it (max 5) to the GM. When the event happens, the GM opens the document and reads it. If the GM agrees that you predicted the event, tell as many Good Tales as you like until the odds are significantly in your favor. 

* *Describe*: Creating a master plan, building contingencies, creating Plan B, preparing for the future.

### Mount

You can gain a minion (max 1) with half of your XP and the contract "_______ will loyally serve as your mount in exchange for basic necessities." 

* *Describe*: Building a mount, calling it from the ether, ordering one from your organization, taming a beast.
* *Mounts*: Horse, unicorn, hippogriff, griffon, manticore, dragon, boar, shark, bear, wolf, lizard, wyvern, spider.

### Reinforce

You can spend resources to fortify a location beyond what is normally possible. Collaborate on what fortifications you can provide, and how you achieve them.

* *Defenses*: Walls withstand siege attacks, there's an anti-magic field, your moat attracts magic beasts.
* *Describe*: Buttressing the structure, casting protective wards, consecrating the land, infusing it with magic.

### Stronghold

You own a heavily defensible, self-sufficient stronghold. Collaborate on the stronghold's properties. If you like, it has staff dedicated to upkeep. They're your minions with the contract "____ will maintain your stronghold in exchange for safely living there." 

* *Describe*: Overseeing your staff and subjects, reinforcing walls, securing territory, negotiating with neighbors.
* *Strongholds*: Wizard tower, keep, fortress, cave system, battleship, tree-top fortress, extradimensional castle.

## Interaction

### Basic Training

You can train your minions to give each 1 XP (max 5 XP per minion).

* *Describe*: Commanding your minions, inspiring them with your presence, leading by example, training them.

### Call in the Cavalry

You can designate some of your minions as cavalry and send them off to wait and prepare. You can have your cavalry arrive at your location, just in time.

* *Describe*: Blowing a war horn, giving a signal, seeing them over your enemy's shoulder, shouting "NOW!"

### Companion

Gain a minion (max 1) with half your XP and the contract "_______ will loyally serve you in exchange for being treated like a friend or trusted servant."

* *Describe*: Keeping a squire, teaching an apprentice, keeping a mercenary on retainer, traveling with a friend.

### Draft

When you acquire minions, you can acquire twice as many as normal. 

* *Describe*: Promising gold and glory, inspiring others to action, leveraging your social connections, paying well.

### Fealty

When you achieve glory, minions who shared in your rewards replace the second half of their contracts with "in exchange for nothing." 

* *Describe*: Appealing to their patriotism, building loyalty, forging bonds of honor, ruling with an iron fist.

### Initiation Vows 
Collaborate on a vow. All of your minions follow that vow, no matter the cost.

* *Describe*: Creating a strong culture, forcing them to swear an oath, binding them by blood.
* *Vows*: Never reveal secrets, uphold honor, defend the leader at all costs, fulfill a deity's dogma, stay hidden.

### Marshal Forces

Collaborate on a special type of minion that you can create beyond what is normally possible, and their contracts. Their disposition toward you is significantly improved.

* *Describe*: Turning corpses into zombies, building robots from scrap, laying eggs that hatch into spawn.
