# Savagery

Might makes right! Don't agree? Try arguing that point after someone stronger than you mounts your head on a pike. Savage warriors embrace the primordial rule of survival of the fittest, relying on instinct and brute strength to destroy their enemies. Some weak-willed people think that civilization leads to safety, but the truth is that laws make men docile. Real power comes from raw, unbridled rage-that roiling fury that makes you want to flip over a table and gouge someone's eyes out. What makes your blood boil, mighty warrior? Are you enraged by the injustices of corrupt societies? Do you sneer defiantly at the sight of a hulking, bloodthirsty monster? Then lift your weapon and show your enemies the true meaning of power!

* A muscled barbarian wanders the frozen tundra, slaying monsters in search of fame and fortune.

* A young boy is found in the wilderness, apparently raised by wolves. He fights with a primal savagery that no civilized man can match.

* After getting hit with the green energy of an arcane spell gone awry, a mild-mannered wizard gains godlike strength when he gets angry.

## Combat

### Bloodlust

Gain the defense "Bloodlust []." Mark it when you suffer a Bad Tale that physically damages you. Then, gain a free Good Tale to make an attack. 

* *Describe*: Using the pain to fuel your rage, reveling in glorious bloody combat, returning the favor. 

### Cleave

You can make a melee attack against as many adjacent creatures as you like. 

* *Describe*: Attacking in a flurry, cutting through foes, spinning in a blade tornado, releasing an explosion.

### Impale

When you attack someone with a melee or throw weapon, you can give your target the condition "Impaled." 

* *Describe*: Pushing a spear through their stomach, embedding your hammer in their armor. 

### Rage

You can give yourself the condition "Enraged." While enraged, increase all rolls to destroy.

* *Describe*: Harnessing the pain, entering a primal rage, attacking recklessly, leaving yourself open to attack.

### Rampage

When you defeat a worthy foe, gain a free Good Tale. 

* *Describe*: Entering a blood-fueled rage, slaughtering foe after foe, standing atop a mountain of corpses.

### Sticks and Stones

Every item you hold has the Melee and Thrown item traits for free, as well as 2 other item traits of your choice. They lose these extra traits when they leave your hands.

* *Describe*: Using whatever's available, clubbing someone with a femur, beating someone with their own arm.

### Unbroken

Collaborate on how you charge this trait. Spend the charge to remove 1 physical condition
from yourself that you can logically remove. Then, gain a free Good Tale. 

* *Describe*: Breaking free, getting an adrenaline rush, screaming in anger, overcoming with your burning rage.

## Exploration

### Gut Instinct

At any point, you can have the GM describe your gut reaction, which gives you vague but accurate information about your surroundings. 

* *Describe*: Getting a bad feeling, thinking that person is off somehow, feeling the hair on your neck stand up.

### Pillage

When you destroy a worthy structure, you can find at least 1 treasure among the rubble.

* *Describe*: Finding gold in the ash and rubble, looting priceless art, taking whatever you want, looting stores.

### Siege Breaker

Your ability to destroy the terrain exceeds normal limits. Collaborate on what you can do. 

* *Describe*: Shoving someone through a wall, chopping a support beam, kicking down the door, smashing a table.

### Superstition

Collaborate on what bring good and bad luck. These superstitions are now true for you and everyone you can see; good luck causes good things to happen, and bad luck causes bad things to happen. 

* *Bad Luck*: Washing off the blood of your enemy, fighting with a broken weapon, dishonoring the dead.
* *Describe*: Following ancient traditions, distrusting modern society, carrying fetishes and good luck charms.
* *Good Luck*: Hunting under a new moon, earning a virgin's kiss, getting punched by a stranger, a foggy dawn.

### Vision Quest

When you start a quest, tell the GM something you want to know, understand, or see. The GM will give you a vision, sign, or epiphany that reveals that information to you. 

* *Describe*: Receiving a message from your ancestors, having a prophetic dream, speaking to your spirit beast.

### What is Best in Life?

Collaborate on 3 things that are best in life. When you complete all 3, you and allies who helped you each gain 1 XP. 

* *Best in Life*: Crush your enemies, see them driven before you, hear the lamentations of their loved ones.
* *Describe*: Reveling in your successes, reflecting on your triumphs, obtaining glory, amassing power.

## Interaction

### No Escape

When someone flees from you or otherwise tries to avoid your wrath, you can tell a free Good Tale against them.

* *Describe*: Calling them out by name, embarrassing them in front of their peers, mocking their honor.

### Instigator

You can choose someone. A fight or conflict breaks out involving them.

* *Describe*: Goading someone into action, spreading rumors, starting trouble, shouting "Fight! Fight! Fight!"

### Intimidating

You are far more intimidating than normal. You can intimidate creatures that would normally wouldn't flinch at threats. Collaborate on how your intimidation exceed normal limitations.

* *Describe*: Threatening them with violence, torturing them, pushing your weight around, exploiting fear.

### Noble Savage

Gain the defense "Noble Savage []." Mark it when you suffer a Bad Tale that relates to laws, high culture, and the trappings of civilization. When you do, choose someone; their disposition toward you significantly improves.

* *Describe*: Your simplistic ways, your adorable naivete, following the laws of nature instead of the laws of man.

### Not Getting Paid Enough

Whenever you do something impressive, you can choose a minion
who watched you. They immediately abandon their contract.

* *Describe*: Proving that they're no match for you, terrifying them with your might, letting them run away.

### To the Victor

When you destroy someone or conquer territory, choose something they had that would normally be difficult for you to obtain. Everyone acknowledges that it's your now. 

* *Describe*: Crushing their hope like you crushed their leader's skull, making an offer they can't refuse.
* *Go the Spoils*: Their minions, their lovers' affection, their legal property, their social status. 

### Warchief

Add "or getting them into a good fight" to the end of your minions' contracts. 

* *Describe*: Indulging their bloodlust, promising them spoils of war, provoking their savage nature.
