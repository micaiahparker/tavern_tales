# Transformation

Adapt to survive; this is a fundamental rule of the universe. Each new sunrise brings with it the chaos of infinite possibilities: your closest friend could betray you; nations could rise or fall; a new discovery could revolutionize our understanding of the universe. You could either futilely cling to the old ways as the future renders you obsolete, or you can adapt, evolve, and transform yourself to thrive in this new world. Shifters embrace the chaos of change. When they gaze into tomorrow, they imagine the countless possibilities of what they could become: king, assassin, lover, hero, villain - everything and anything. These shifters understand that chaos flows both ways. The future has the power to change us, but we also have the power to change the future. Are you strong enough to change the world?

* A soldier turns into a vicious werewolf under the light of a full moon.

* A master spy shapeshifts into members of royalty to spy on the government.

* After transcending the mortal coil, a spiritual monk embraces no form by growing multiple limbs.

## Combat

### Clone

You can create a clone of yourself that's a separate character you control. Collaborate on how this trait works.

* *Describe*: Splitting in half, popping into existence next to yourself, growing a clone in a tube, self-replicating.
* *How it Works*: You have to grow your clones in a lab, they're treacherous, the process gives you a condition.

### Doppelganger

Write your personal XP total on your character sheet. When you kill a creature, you can have the GM give you their character sheet. You become identical to them in every way and use their character sheet. Transfer your gear, personal XP, and this trait to the new character sheet. If your new character sheet has more XP than your personal XP, gain a condition that you can't remove until you change character sheets. You can return to your original character sheet at any point, but you forever lose the stolen character sheet. 

* *Describe*: Stealing their whole identity, changing your form, absorbing their essence, changing skin.

### Evolve

When you retrain, you can lose a stat, trait, or upgrade to refund all of the XP you spent to purchase it. Spend that XP however you like. 

* *Describe*: Adapting to the environment, growing from your experiences, incorporating foreign DNA.

### Mimic

When you see a creature use a trait, you can gain that trait (max 1) as a temporary trait for several hours. 

* *Describe*: Copying whatever they do, assimilating their DNA, reactively adapting, evolving, changing your soul.

### Morph Item

You can replace 1 item trait on an object you're touching with a different item trait. 

* *Describe*: Adapting to your weapon, bending the wood and metal, transforming it into an entirely new item.

### Polymorph

You can change a willing or defeated creature into a different creature. They refund all of their XP, which slowly returns over a few days or weeks. They can spend it however they want to suit their new form. 

* *Describe*: Hexing them with dark magic, reshaping skin and bones, mutating them, reweaving their soul.

### Regenerate

You heal about twice as fast as normal. Also, you heal things that are otherwise unhealable.

* *Describe*: Wounds spontaneously closing, regrowing an arm, stitching your flesh together, magically healing.
* *Unhealable Things*: Amputated hand, lost eye, missing teeth, burn scars, tumorous growths, paralysis.

### Shapeshift

Create a second character sheet that always has the same XP as your main character. You can shift forms, changing character sheets in the process. Keep your conditions, marked boxes, and gear. Collaborate on how this trait works.

* *Describe*: Twisting and reshaping your skin, rapidly evolving, switching places with your shadow.
* *How it Works*: You can't control when you transform, your alternate form is bloodthirsty, shifting drains you.

## Exploration

### Alter Size

You can change your size for a few hours. Collaborate on how much you can change your size.

* *Describe*: Warping space around you, accelerating your metabolism, harnessing your titan blood. 

### Chameleon Skin

If you're perfectly motionless, you're invisible.

* *Describe*: Surrounding  yourself in an illusion, changing your skin color, becoming transparent.

### Chaos Theory

You can do something mundane to begin a chain of events (max 1). Every 30 minutes or so (real-time), the next player to the left rolls to determine how the chain of events escalates. Good Tales make the chain better; Bad Tales make the chain worse. At any point, you can make a final roll to end the chain of events. 

* *Begin the Chain*: Pet a dog, give a beggar a gold coin,  drop a nail, fire an arrow randomly into the distance.
* *Describe*: Manipulating events, embracing the random chaos of the universe, relying on pure luck.End the Chain: Help arrives, something explodes, someone dies, a building collapses, a war starts.

### Clay Body

Your body can be malleable like clay or rubber.

* *Describe*: Stretching your muscles, popping your bones out of joint, becoming like clay or water, bending.
* *Things You Can Do*: Squeeze through a keyhole, reach a distant item, pull your hands out of manacles.

### Perfect Replica

You can spend materials to create a perfect duplicate of an item. 

* *Describe*: Casting a flawless illusion, crafting something with pure artistry, creating a magic replica.
* *Materials*: Whatever the original item was made of, wood, steel, textiles, earth and stone, magic dust.

### Shape Flesh

You can transform into an object or back to your original form. While you're an object, you retain your senses and you can't act except to use this trait.

* *Describe*: Morphing, changing your skin, warping reality, transferring your soul to a physical vessel.

### Transmute

You can change an object's properties, or change an object into a similar object.

* *Describe*: Morphing, changing physics, warping reality, rearranging molecules, transmuting elements.
* *Properties*: Hardness, transparency, weight, density, color, smell, texture, size, conductivity, buoyancy.
* *Similar Objects*: Sand into stone, coal into oil, wood into paper, ore into ingots, seawater into fresh water.

## Interaction

### A Thousand Masks

You can change your appearance to another similarly sized creature. Collaborate on how this trait works. 

* *Describe*: Putting on a perfect disguise, changing bodies, morphing your phase, creating an illusion.
* *How it Works*: You must have touched them before, you leave a telltale flaw, you can't sustain it for long.

### Change of Heart

You can reverse part of a creature's personality for several hours.

* *Describe*: Warping their mind, inverting brain waves, reversing the polarity, changing their alignment.
* *Reverses*: Trust into mistrust, hate into love, curiosity into disinterest, lawfulness into lawlessness.

### Cosmic Trade

You can permanently trade parts of yourself with a willing creature.

* *Describe*: Switching parts of your souls, swapping body parts, changing bodies, reshaping reality.
* *Trade*: Faces, bodies, stats, traits, personalities, minds, destinies, traits, conditions, beauty for brains.

### Gestalt

You can fuse with other willing creatures and become a single entity with all of your combined strengths and weaknesses. Collaborate on what this entails. This ends when any participant wants out.

* *Describe*: Fusing with gem magic, absorbing their bodies, initiating morphing time, forming the head.
* *Entails*: Takes less damage, gain access to all traits, grow in size, use the highest stats from each person.

### It Was Me All Along!

If nobody knows where you are, you can reveal that an unimportant NPC was secretly you all along.

* *Describe*: Pulling back your hood, taking off your mask, morphing to your true form, staging a grand reveal.

### Takes One to Know One

While you are impersonating someone, the GM must answer all of your questions about your role's mannerisms, habits, and knowledge if the answers would help you perform that role. Explain how you know this information.

* *Describe*: Copying body language perfectly, creating a flawless disguise, studying your mark, acting.

### The Curse

Collaborate on how you curse others. When you curse someone, refund half of their XP. Change their aesthetics and spend their XP however you like to make them more like you. Their disposition toward you significantly improves.

* *Describe*: Infecting their bloodstream, hexing them with magic, leaving a mark on their skin, cursing them.
* *Spread By*: Draining their blood, biting them, giving them a parasite, performing a tribal ritual.
* *Transform Into*: Vampire, werewolf, zombie, ghost, horrific Old One, assimilated insectoid, drone.

### Transcendent Voice

You can perfectly replicate any sound you've heard before, including volume.

* *Describe*: Perfectly mimicking a sound, recording a sound in you mind, transforming your vocal cords. 
