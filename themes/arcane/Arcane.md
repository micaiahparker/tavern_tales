
# Arcane

There are some who seek to look beyond the veil to unravel the mysteries of the universe, unlocking ancient power that was never intended to fall into mortal hands. These masters of the arcane arts bend the universe to their will by breaking the laws of physics and inventing new laws at whim. They fling fireballs as easily as an archer might launch an arrow. They warp space and time to create portals that span across continents. They bind their enemies under powerful spells to leave them as helpless as newborn kittens. Do you dare to rip apart the fabric of spacetime to tinker with reality? Unfathomable power awaits those foolish or brave enough to try.
 
* An aged wizard spends his days poring over tomes to create a new spell.
* A war mage devastates his foes with orbs of raw energy and powerful hexes.
* A scientist researches bizarre magical anomalies hoping for the next big scientific breakthrough.


## Combat

### Barrier

You can create a wall (max 1) of 20 spaces or less that lasts a few minutes. Shape it however you want, but don't imprison anyone. 

* *Describe*: Creating a wall of force, conjuring a wall of iron, dividing reality with an impenetrable barrier.

### Counterspell

Collaborate on how you charge this trait. At any point, spend the charge to make someone's Tale utterly fail. 
* *Charge*: Write runes in your spellbook, enchant your staff with a null-spell, meditate in total silence.
* *Describe*: Turning their power against them, activating a ward, triggering a contingency, casting an anti-spell.

### Dispel

You can destroy or suppress magic.

* *Describe*: Stealing magic, purging dark magic, teleporting energy, reshaping reality, siphoning magic.

### Evocation
You can create an explosion in a zone.

* *Describe*: Explosion of arcane energy, beam of pure energy, cone of force, burst of raw chaos all around you.

### Magic Resistance

Gain the defense "Magic Resistance []." Mark it when you suffer a Bad Tale related to magic. You're then immune to that specific magic for several minutes. 

* *Describe*: Conjuring a spell shield, your magical blood, have a resistant spirit, your spell-ward tattoos. 
 
### Magic Missile

You don't need ammo to make ranged attacks, and your projectiles can turn corners to seek targets.

* *Describe*: Conjuring orbs of energy, shooting blue lights, firing laser beams, creating ammo from the ether. 

### Metamagic

Collaborate on a resource associated with this trait, and how you acquire that resource. You can have 3 of that resource at a time. Spend 1 resource to choose 1 effect for your action:

* It's much subtler or flashier. 
* Approximately double the range.
* Approximately double the duration.
* Delay it from activating for a few minutes.

* *Describe*: Infusing your spells with mana, activating runes, shouting words of power, using crystals.
* *Resources*: Meditate to gather mana, inscribe symbols in a spellbook, gather energy from magical leylines.

### Rewind

You can rewind someone, returning them to their status about 1 minute ago. Then, you can't use Rewind again for about 1 minute. 

* *Describe*: Turning back time, jumping into a worm-hole, reversing time's flow, casting chrono-magic.
* *Status*: Inventory, wounds, location, treasure, temporary conditions, memories, physical condition.

### Warp Time

You can accelerate or decelerate someone for a few minutes. Accelerated creatures move about twice as fast as normal. Decelerated creatures move about half as fast as normal. 

* *Describe*: Casting a chronomancy spell, reversing the flow of time, opening or closing the time stream.

## Exploration

### Alter Time

You can alter time. 

Collaborate on what you do.

* *Alter Time*: Undo an event, slow down time in an area, travel to the far future or distant past, restore youth.
* *Describe*: Changing the flow of times, casting powerful chrono-magic, stepping out of the time stream.

### Cantrips 

You can perform minor magic tricks. You can do virtually anything with these tricks so long as the effect is comparatively weak.

* *Describe*: Performing sleight of hand, creating a minor illusion, dazzling with showy magic, casting rote spells.
* *Tricks*: Inscribe a permanent glowing rune, make a handheld item vanish or appear, light candles in a room.

### Detect Magic

You can experience magic with your normal senses.

* *Describe*: Seeing swirls of magic, how magic sounds, the stench of necromancy, the glow of divine magic.

### Illusion

You can create a convincing illusion that lasts for several hours.

* *Describe*: Bending light and shadow, creating a faerie illusion, deceiving their minds, casting twilight magic.

### Invisibility

You can touch something to turn it invisible for several hours. Collaborate on what causes the invisibility to flicker, fade, and end.

* *Describe*: Bending light and shadow, creating a faerie illusion, deceiving their minds, casting twilight magic.

### Place of Power

Whenever you encounter a place of power, collaborate on what power you can draw from it. 

* *Describe*: Pulling forth a geyser of pure energy, devouring raw magic, bending energy to your will.
* *Places*: Ley line, magic anomaly, dragon graveyard, primeval grove, heart of the mountain, sun temple.
* *Power*: Amplify a trait, gain a temporary trait, deal more damage there, heal more quickly, enhance a stat.

### Ritual

Whenever you want, tell the GM a powerful effect you want to achieve. The GM will tell you what you need to do to complete a ritual. If you fulfill the requirements,  you achieve the desired effect.

* *Describe*: Performing an ancient ritual, reciting magic phrases, chanting around a glowing circle.
* *Effects*: Eternally seal a creature in a prison, raise a sunken ship, make a magic effect permanent.
* *Requirements*: Visit a special location, sacrifice treasure, chant with other wizards, obtain a special item. 

### Teleport

You can teleport. 

Collaborate on what that entails.

* *Describe*: Vanishing in a puff, rearranging spacetime, opening a wormhole, stepping across dimensions. 
* *How it Works*: You must have been there before, requires line of sight, requires intense concentration.

### Wizard Eye

You can create an eye-sized item. You can see, hear, and sense through it whenever you want. It's permanent and immobile, or it flies wherever you want and lasts for about 1 hour.

* *Describe*: Conjuring a floating disembodied eye, enchanting a crystal ball, activating a magic drone.

## Interaction

### Apprentice 

You can designate 1 of your minions (max 1) as your apprentice. Their disposition toward you significantly improves and the end of their contract becomes "in exchange for guidance and hands-on training." When you gain XP, your apprentice gains that much XP as well. 

* *Describe*: Leading by example, explaining the intricacies of your craft, testing their skills, bonding with them.

### Familiar

Create a minion with half of your XP and the contract "_______ will loyally serve as your lab assistant, pet, and familiar in exchange for basic necessities." If lost, collaborate on how you replace it.

* *Describe*: Summoning a familiar, growing it from a drop of your blood, enchanting an animal to serve you.
* *Familiars*: Imp, cat, frog, snake, mouse, dog, monkey, small robot, raven, owl, drakeling, mana dragon.

### Flawless Logic

You are far more persuasive with logic, reasoning, and evidence. You can sway others in situations when logic would normally fall on deaf ears. Collaborate on how your logical arguments exceed normal limitations.

* *Describe*: Listing the facts, reaching a logical conclusion, using your sharp mind, outwitting someone.

### Gentleman and a Scholar

When you show someone respect, they will match the amount of respect you show them until you show them disrespect. 

* *Describe*: Wearing a monocle, bowing graciously, giving a firm handshake, showing due respect.

### Just an Old Man with a Walking Stick

You can make someone grossly underestimate you until they see proof to the contrary.

* *Describe*: Leaning heavily on your cane, acting awkward and socially inept, acting naive and harmless.

### Mutual Edification

When you honestly reveal information to someone or answer one of their questions, they must share equally important information or answer an equally sensitive question.

* *Describe*: Proposing an exchange, tricking them into talking, showing scholarly respect, educating each other.

### Pedantic

You can make people listening to you gain the condition "Bored and Sleepy." 

* *Describe*: Talking in one long incredibly unbroken sentence, your monotone voice, droning on and on.
* *Topics*: Spore varieties, extended noble lineage, Agazar's Third Law of Arcano-Kinetics, ancient texts.

### Polyglot

Collaborate on which additional languages you know.

* *Describe*: How you learned another language, the language's accent, speaking a sentence in the language.

