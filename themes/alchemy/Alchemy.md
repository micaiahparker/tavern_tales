# Alchemy

Many see alchemy as the study of matter; others view alchemy as the study of change. In truth, both are correct, for alchemy is the study of everything. What doesn't fall within its domain? Everything in the universe — from the tiniest speck of dust to the massive, ever-burning sun — owes its existence to chemistry. If matter and energy are the building blocks of reality, then alchemists are grand cosmic architects who rival even the gods themselves in their mastery over the physical world. Alchemists destroy matter in a way that would humble even the most fearsome warlord, breaking down objects into their most fundamental pieces. Alchemists create new wonders in ways that artists could never hope to match, brewing new chemicals that were heretofore unseen. When you understand the inner workings of the universe, the possibilities are endless: you could bottle sunlight, brew glory, or even unlock the hidden process for turning lead into gold. So, master chemist, what will you brew into your next bubbling concoction? 

* A hermit lives in the swamp, where he brews fish eyes, pickled fireflies, and muddy water into potent elixirs.

* A chemist travels the world in search of rare and exotic materials to create the elixir of eternal youth.

* Hidden in her secret lair, an assassin creates deadly poisons for her next kill.

## Combat

### Acid

You can dissolve a handheld item or an equivalent amount of material. 

* *Describe*: Sizzling chemical, acrid scent, bubbling green liquid, reducing something to frothy sludge.

### Biohazard

You can expend a chemical that you did not create to tell a free Good Tale. 

* *Chemicals*: Black powder barrel, vial of acid, goo pud-dle, mushrooms, pool of monster blood, thick smoke.
* *Describe*: Causing a chemical reaction, adding a spark to explosives, identifying a chemical's properties.
* *Reactions*: Explode, catch something on fire, heal some-one, blind, create a thick cloud, poison, create fireworks.

### Flashbang

You can create an incredibly bright flash and loud bang in a zone. Creatures in the zone gain the condition "Blind and deaf" until the effects fade. 

* *Describe*: Bottled sunlight, sonic crystals, volatile chemicals, shattering a sun crystal, fireworks, a bright flare.

### Healing Potion

You can spend reagents to create a healing potion. Anyone who drinks it heals as if they received 1 day of uninterrupted bed rest and medical attention. Then, they gain the condition "Hungry and tired." 

* *Describe*: A bubbling red liquid, a sweet-tasting potion, medicinal salves, brewing the potion in your lab. 
* *Reagents*: Curative herbs, pure water, bottled sunlight, tree sap, crushed fruit seeds, enriched blood, sugar.

### Inoculated

Gain the defense "Inoculated []." Mark it when you suffer a Bad Tale related to chemicals. Also, you're immune to poison, disease, and related conditions.

Conditions: Disease, poison, venom, parasites, infec-tions, blood curse, forced transformations.

### Panacea

You can spend reagents to create a panacea. Anyone who drinks it replaces all conditions related to physical ailments with "Hungry and tired." Physical ailments include things like poisons, diseases, and curses, not wounds like broken bones or severed arteries.

* *Describe*: Distilled water so pure it washes away any-thing, drink an antitoxin, release cleansing vapors.
* *Conditions*: Disease, poison, venom, parasites, infec-tions, blood curse, forced transformations. 
* *Reagents*: Distilled water, bottled cloud, soap, clear parchment pulp, spring water, a new leaf, starlight.

### Poisonous

You have a poison. Collaborate on its properties. Creatures you poison gain the condition "Poisoned" and slowly start to suffer symptoms. 

* *Describe*: Injecting poison, venomous fangs, a toxic vial, poison-tipped blow darts, venom-coated weapon.Symptoms: Muscle weakness, nausea, eventual death, paralysis, hallucinations, paranoia, anti-coagulation.

### Tranquilizer

You have a tranquilizer. Collaborate on its properties. Creatures you tranquilize gain the condition "Tranquilized" and slowly start to lose faculties. 

* *Describe*: Brewing the chemical in your lab, loading it into a delivery system, how their senses go numb. 
* *Faculties*: Consciousness, sight, sense of feeling, ability to move, ability to talk, a certain trait, emotions.

## Exploration

### Adamanthesive

You can adhere two things together. After about 1 minute, they're permanently bound together. Collaborate on what dissolves the adhesive.

* *Describe*: Alchemical glue, fusing molecules together, never-melting ice, stitching together the fabric of reality.
* *Dissolves By*: Time, extreme heat or cold, a certain chemical, a magic phrase, pure distilled water, starlight.

### Field Alchemy

You can harvest rare reagents. When you do, the GM tells you what trait the reagent contains. You can perform alchemy to transform the reagent into an elixir. Anyone who drinks it gains that trait as a temporary trait for several minutes.

* *Describe*: Mixing chemicals in a lab, distilling essence, bubbling liquids, growing magic crystals, mad science.
* *Reagents*: Moss, monster blood, powdered monster bone, venom, mushrooms, tree sap, magic crystals.

### Mutagen

You can spend reagents to create a mutagen, which can cause any effect imaginable. Collaborate on your mutagen's effects. For every beneficial effect your mutagen has, the GM gives it a detrimental side-effect.

* *Describe*: Brewing a bubbling potion, harnessing raw chaos, bottling a chain reaction, special lab equipment.
* *Effects*: Reduce a building to acidic sludge, transform someone into a monster, cause a HUGE explosion.
* *Reagents*: Unstable chemicals, powerful bases, caustic acids, radioactive metal flakes, mithril flakes.
* *Side-Effects*: It's highly unstable, its radioactive, you mutate hideously, the fumes are toxic, it drains you.
 

### Oil Slick

You can create an extremely slippery zone. 

* *Describe*: Spew oil from a hose, shatter a goo flask, se-crete from oily glands, shatter an oil barrel.

### Philosopher's Stone

You can turn certain materials into treasure. Collaborate on what materials you can transform.

* *Describe*: Changing their chemical structure, reshaping reality, turning lead into gold, rebuilding atoms.

### Thermite

You can spend reagents to create thermite. Anyone can spend about 1 minute setting it up to activate it. Once activated, it slowly and continuously burns a one-space tunnel through absolutely anything in whatever direction the user likes for several minutes. Thermite is absolutely unstoppable until it naturally burns out.

* *Describe*: Billowing smoke, choking miasma, chemical fumes, bubbling chemicals, the acrid scent.
* *Reagents*: Metal oxide, rust flakes, copper powder, gun powder, kindling, bottled flame, lamp oil, aluminum.

## Interaction

### All-cohol

You can spend reagents to create all-cohol. Anyone who drinks so much as a sip instantly gains the condition "Drunk" and must act accordingly.

* *Describe*: Distilling the alcohol, the fermentation pro-cess, ruddy cheeks and slurred speech, how it tastes.
* *Reagents*: Yeast, barley, pure alcohol, liquor, wheat, hops, grains, distilled water, grapes, ripe fruit.

### Drug

You can spend reagents to create an addictive drug. Collaborate on the drug's properties.

* *Administered By*: Ingestion, inhaling via smoke, injec-tion, inhalation through the nose, skin contact.
* *Describe*: Growing special plants, brewing in your lab, wearing a gas mask and gloves, breaking bad.
* *Properties*: Extreme elation, hyperactivity, absolute fearlessness, itchy hives, sleeplessness, the munchies.
* *Reagents*: Herbs, roots, crushed leaves, powdered seeds, dissolved tree bark, lizard oil, swamp gas, fumes.

### Love Potion

You can spend reagents to create a love potion. Anyone who drinks it gains the condition "Enamored" and falls in love with the next person they see who is of a race and gender they normally find attractive. 

* *Describe*: Light and fruity pink liquid, butterflies in their stomach, distilling the essence of desire.
* *Reagents*: Butterfly wings, wine, strawberries, choco-late, hair from a virgin, sugar, sweat, spring water. 

### Perfume

You can give something an incredibly pleasant or unpleasant scent for about 1 day. If it's pleasant, creatures are inexplicably drawn to the scent and gain the desire to possess the scent's source. If it's repulsive, creature are disgusted by it and don't want to be near it. This may have the opposite effect on creatures like undead and vermin that are attracted to disgusting smells. 

* *Describe*: Heavenly scent, heady aroma, relaxed and pleasant feeling, euphoria, being light headed.

### Pheromones

Collaborate on a base emotion or instinct. You can emit pheromones in the air. Anyone who breathes in your pheromones gains the condition "Instinct-Driven" and experiences the selected emotion or instinct with increasing intensity.

* *Describe*: Musky scent, dilated pupils, mixing animal pheromones, working in your lab, altered biochemistry.
* *Emotions and Instincts*: Survive, reproduce, find a pack, lust, fear, rage, hunger, envy, joy, sadness.

### Snake Oil Salesman

You have a 50% discount on all purchases, and a 50% markup on all sales.

* *Describe*: Flashing a warm smile, giving a sales pitch, haggling down the price, "But wait, there's more!"
